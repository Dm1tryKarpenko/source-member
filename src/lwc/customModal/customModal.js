/**
 * Created by vladislav on 17.09.21.
 */

import {LightningElement, api } from 'lwc';

export default class CustomModal extends LightningElement {
    @api
    textContent= '';
    @api
    textCancel = 'Cancel';
    @api
    textSave = 'Save';

    _textHeader = 'Modal';

    _size = ''; // small, medium, large

    @api
    isHideFooter = false;
    @api
    isHideCancel = false;
    @api
    isDisableCancel = false;
    @api
    isDisableSave = false;
    @api
    isLoading = false;

    _isHideHeader = false;
    _isDirectional = false;
    _isHideModal = false;


    classContainer = 'slds-modal slds-fade-in-open';
    classBackdrop = 'slds-backdrop slds-backdrop_open';
    classHeader = 'slds-modal__header';
    classFooter = 'slds-modal__footer';


    @api
    get textHeader() {
        return this._textHeader;
    }

    set textHeader(value) {
        this._textHeader = value;
    }

    @api
    get isHideHeader() {
        return this._isHideHeader;
    }

    set isHideHeader(value) {
        this._isHideHeader = value;
        this.classHeader = !value ? 'slds-modal__header' : 'slds-modal__header slds-modal__header_empty';
    }

    @api
    get isDirectional() {
        return this._isDirectional;
    }

    set isDirectional(value) {
        this._isDirectional = value;
        this.classFooter = value ? 'slds-modal__footer slds-modal__footer_directional' : 'slds-modal__footer';
    }

    @api
    get isHideModal() {
        return this._isHideModal;
    }

    set isHideModal(value) {
        this._isHideModal = value;
        this.classContainer = value
            ? this.classContainer.replaceAll('slds-fade-in-open', '')
            : this.classContainer + ' slds-fade-in-open';
        this.classBackdrop = value
            ? this.classBackdrop.replaceAll('slds-backdrop_open', '')
            : this.classBackdrop + ' slds-backdrop_open';
    }


    @api
    get size() {
        return this._size;
    }

    set size(value) {
        this._size = value;
        let classContainer = this.classContainer.replaceAll(/slds-modal_(small|medium|large)/g,'');
        switch (value) {
            case 'small' :
                classContainer += ' slds-modal_small';
                break;
            case 'medium' :
                classContainer += ' slds-modal_medium';
                break;
            case 'large' :
                classContainer += ' slds-modal_large';
                break;
        }
        this.classContainer = classContainer;
    }

    handleClose(event) {
        this.dispatchEvent(new CustomEvent('close', { detail: { type: 'close' } }));
    }

    handleCancel(event) {
        this.dispatchEvent(new CustomEvent('cancel', { detail: { type: 'cancel' } }));
    }

    handleSave(event) {
        this.dispatchEvent(new CustomEvent('save', { detail: { type: 'save' } }));
    }



}