/**
 * Created by vladislav on 6.09.21.
 */

import {LightningElement, api} from 'lwc';

export default class ComboboxClassic extends LightningElement {
    @api label = false;
    @api placeholder = '';
    _options = [];
    _value = '';


    @api
    get value() {
        return this._value;
    }

    set value(val) {
        this._value = val;
        this.setSelectedOption();
    }

    @api
    get options() {
        return this._options;
    }

    set options(val) {
        this._options = val;
        this.setSelectedOption();
    }

    setSelectedOption() {
        this._options = this._options.map((item) => item.value === this._value ? { ...item, selected: true } : { ...item, selected: false });
    }

    handleChange(event) {
        this.dispatchEvent(new CustomEvent('change', {detail: {value: event.target.value}}));
    }
}