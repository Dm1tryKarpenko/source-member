/**
 * Created by vladislav on 20.10.21.
 */

import {api, LightningElement, track, wire} from 'lwc';
import authorize from "@salesforce/apex/TrackingOrganizationController.authorize";
import getVFOrigin from "@salesforce/apex/TrackingOrganizationController.getVFOrigin";
import getNamespacePrefix from "@salesforce/apex/TrackingOrganizationController.getNamespacePrefix";
import launchManualRetrieve from "@salesforce/apex/TrackingOrganizationController.launchManualRetrieve";
import getOrgComponentsForManualRetrieve from "@salesforce/apex/TrackingOrganizationController.getOrgComponentsForManualRetrieve";
import updateLog from "@salesforce/apex/TrackingOrganizationController.updateLog";
import trackingSettingAbortJob from "@salesforce/apex/TrackingOrganizationController.trackingSettingAbortJob";
import updateTooLargeComponents from "@salesforce/apex/TrackingOrganizationController.updateTooLargeComponents";
import finishManualRetrieve from "@salesforce/apex/TrackingOrganizationController.finishManualRetrieve";
import {
    convertToJSON,
    TOAST_TYPE_SUCCESS,
    TOAST_TYPE_ERROR,
    TOAST_TYPE_WARNING,
} from 'c/trackingOrganizationHelper';

const TRACKING_HELPER_PAGE = 'TrackingHelper';
const TABLE_TOO_LARGE_SETTINGS = {
    isSupportSelect: true,
    recordsPerPage: 10,
    defaultSortField: 'componentName',
    paginationLabel: 'component',
    columnList: [
        {
            label: 'Component Name',
            field: 'componentName',
            isSorted: true,
            size: 3,
            type: 'string'
        },
        {
            label: 'Component Type',
            field: 'componentType',
            isSorted: true,
            size: 3,
            type: 'string'
        },
        {
            label: 'Changed By',
            field: 'changedBy',
            isSorted: true,
            size: 2,
            type: 'string'
        },
        {
            label: 'Changed On',
            field: 'changedOn',
            isSorted: true,
            size: 2,
            type: 'date'
        },
        {
            label: 'Status',
            field: 'statusLabel',
            isSorted: true,
            size: 2,
            type: 'string'
        }
    ],
    componentList:[]
}
export default class TrackingRetrieveManually extends LightningElement {

    @api recordId;
    // @api vfOrigin;


    @wire(getVFOrigin) vfOrigin;

    @track accessToken = null;
    @track instanceUrl = null;

    @track urlTrackingHelperPage = ''; // /apex/TrackingHelper

    @track queueManualFunctions = [];

    @track tooLargeSettings = TABLE_TOO_LARGE_SETTINGS;

    @track _log = null;
    @track _componentList = [];

    @track isLoading = true;
    @track isShowModalKillProcess = false;
    @track isHideModal = false;
    @track isManualRetrieveProcess = false;
    @track isDisableButtonManualPull = true;
    @track isInProgressKillProcess = false;


    @api
    get log() {
        return this._log;
    }

    set log(value) {
        if (this.isManualRetrieveProcess || this._log) {
            const content = this.template.querySelector(`div[data-id="activeManualLog"] div[data-id="${value.contentId}"]`);
            if (content && value) {
                value = {...value};
                const logBodyBlock = content.querySelector(`div.log-body-block`);
                const logBody = this.isClassic ? content.querySelector(`div.log-content-body`) : logBodyBlock;
                const autoscroll = content.querySelector(`.autoscroll`);
                value.isAutoscroll = autoscroll ? autoscroll.checked : false;

                if (logBody && logBodyBlock) {
                    logBodyBlock.innerHTML = value.body;
                    if (value.isAutoscroll) logBody.scrollTop = logBody.scrollHeight;
                }
            }
            this._log = value;
        }
    }

    @api
    get componentList(){
        return this._componentList;
    }

    set componentList(value) {
        this._componentList = value ? value : [];
        this.tooLargeSettings.componentList = this._componentList;
    }


    connectedCallback() {
        Promise.resolve()
            .then(() => window.addEventListener("message", this.handleVFResponse.bind(this)))
            .then(() => this.callAuthorize())
            .then(() => this.callNamespacePrefix())
            .then(() => this.isLoading = false)
            .catch((e) => {
                this.isLoading = false;
                this.showToast(e, TOAST_TYPE_ERROR);
                this.handleCloseModal();
            })
    }

    callAuthorize() {
        return new Promise((resolve, reject) => {
            try {
                authorize({ dataMap: { organizationId: this.recordId } })
                    .then((authorizeDetailsString) => convertToJSON(authorizeDetailsString))
                    .then((authorizeDetails) => {
                        if (authorizeDetails.isSuccess) {
                            this.accessToken = authorizeDetails.accessToken;
                            this.instanceUrl = authorizeDetails.instanceUrl;
                            resolve();
                        } else {
                            reject(authorizeDetails.errorMessage);
                        }
                    })
                    .catch((e) => {
                        reject('Error while Authorization.');
                    })
            } catch (e) {
                reject('Error while Authorization.');
            }
        });
    }

    callNamespacePrefix() {
        getNamespacePrefix()
            .then((result) => {
                let namespacePrefix = '';
                if (result && result.length > 0) namespacePrefix = result + '__';
                this.urlTrackingHelperPage = '/apex/' + namespacePrefix + TRACKING_HELPER_PAGE;
            });
    }

    handleCloseModal(event = {}) {
        if (this.isManualRetrieveProcess) {
            if (this.isInProgressKillProcess) return;
            this.isShowModalKillProcess = !!this.log;
            this.isHideModal = !!this.log;
        } else {
            const endMessage = event.detail ? event.detail.endMessage : null;
            this.dispatchEvent(new CustomEvent('close', { detail: { type: 'close', endMessage } }));
        }
    }

    handleManualPullSelect(event) {
        const selectIdList = event.detail.selectedIdList;
        this.isDisableButtonManualPull = !selectIdList.length;
    }

    handleCloseModalKillProcess(event = {}) {
        this.isShowModalKillProcess = false;
        this.isHideModal = false;
    }

    handleKillProcess(event) {
        this.handleCloseModalKillProcess();
        this.isInProgressKillProcess = true;
        this.finishManualRetrieve(this.log.id, 'Retrieve was killed!');
    }

    handleManualPull(event = {}) {
        if (this.isManualRetrieveProcess || this.log) return;
        this.isDisableButtonManualPull = true;
        this.isManualRetrieveProcess = true;
        this.isLoading = true;
        this.queueManualExecute();

        const componentIdList = new Set(this.template.querySelector(`[data-id="too-large-table"]`).getSelectedComponentIds());
        let componentList = componentIdList.size
            ? this.componentList.filter((item) => componentIdList.has(item.id))
            : this.componentList;

        Promise.resolve()
            .then(() => this.callLaunchManualRetrieve(componentList))
            .then(() => this.showToast('Retrieve Manually launched successfully!', TOAST_TYPE_SUCCESS))
            .then(() => this.waitActiveLog())
            .then(() => {
                this.isLoading = false;
            })
            .then(() => this.waitRetrieveCurrentJobs())
            .then((orgComponentList) => {
                this.retrieveComponentsFromJsForce(orgComponentList);
            })
            .catch((e) => {
                if (e === 'Retrieve already in Process, please try again later.') {
                    this.isDisableButtonManualPull = false;
                    this.isManualRetrieveProcess = false;
                    this.isLoading = false;
                    this.showToast(e, TOAST_TYPE_WARNING);
                } else if (this.isManualRetrieveProcess && !this.isInProgressKillProcess) {
                    this.finishManualRetrieve();
                    this.showToast(e, TOAST_TYPE_ERROR);
                }
            });
    }

    callLaunchManualRetrieve(componentList = []) {
        return new Promise((resolve, reject) => {
            const list = componentList.filter((item) => item.memberId && item.memberId.length).map((item) => item.memberId);
            launchManualRetrieve({ dataMap: { organizationId: this.recordId, componentIdList: JSON.stringify(list) } })
                .then((isLaunchRetrieve) => {
                    if (isLaunchRetrieve === 'true') {
                        resolve();
                    } else{
                        reject('Retrieve already in Process, please try again later.');
                    }
                })
                .catch((e) => {
                    reject('Was error while trying launch manual retrieve');
                });
        });
    }

    waitRetrieveCurrentJobs() {
        return new Promise((resolve, reject) => {
            getOrgComponentsForManualRetrieve({ dataMap: { organizationId: this.recordId } })
                .then((orgComponentJSON) => convertToJSON(orgComponentJSON))
                .then((orgComponentList) => {
                    if (orgComponentList) {
                        resolve(orgComponentList);
                    } else {
                        setTimeout(() => {
                            this.waitRetrieveCurrentJobs()
                                .then((orgComponentList) =>{
                                    resolve(orgComponentList);
                                })
                                .catch((e) => reject(e));
                        }, 1000);
                    }
                })
                .catch((e) => {
                    reject(e);
                });
        });
    }

    waitActiveLog(timer = 20) {
        return new Promise((resolve, reject) => {
            if (this.log) resolve();
            else if (timer > 0) setTimeout(() =>
                    this.waitActiveLog(--timer).then(resolve).catch(reject),
                1000
            );
            else reject('Active Log not found.');
        });
    }


    retrieveComponentsFromJsForce(orgComponentList) {
        if (!this.log) return Promise.reject('Active Log not found');
        let message = { type: 'manual-pull' ,  data: {
                orgComponentList,
                accessToken: this.accessToken,
                instanceUrl: this.instanceUrl,
                activeLogId: this.log.id,
                sourceId: this.recordId
            } };
        const frame = this.template.querySelector('[data-id="tracking-helper"]');
        frame.contentWindow.postMessage(JSON.stringify(message), this.vfOrigin.data + this.urlTrackingHelperPage);
    }

    finishManualRetrieve(logId, successMessage = 'Retrieve completed.') {
        if (!this.isManualRetrieveProcess) return;
        this.isLoading = true;
        if (!logId) logId = this.log ? this.log.id : '';
        this.queueManualFunctions.push(() => {
            return new Promise((resolve, reject) => {
                Promise.resolve()
                    .then(() => trackingSettingAbortJob({dataMap: { organizationId: this.recordId }}))
                    .then(() => updateTooLargeComponents({dataMap: { organizationId: this.recordId }}))
                    .then(() => finishManualRetrieve({dataMap: { organizationId: this.recordId, metadataLogId: logId, finishMessage: successMessage }}))
                    .then(() => this.handleRefreshComponents())
                    .then(() => {
                        this.showToast(successMessage, TOAST_TYPE_SUCCESS);
                        this.isManualRetrieveProcess = false;
                        this.isShowModalKillProcess = false;
                        if (this.isInProgressKillProcess) this.handleCloseModal({ detail: { endMessage: { message: successMessage, type: TOAST_TYPE_SUCCESS } } });
                        this.isLoading = false;
                        resolve();
                    })
                    .catch((e) => {
                        this.showToast('Retrieve was Error', TOAST_TYPE_ERROR);
                        this.isManualRetrieveProcess = false;
                        this.isShowModalKillProcess = false;
                        this.isLoading = false;
                        //console.log('---finishManualRetrieve error', e);
                        reject();
                    });
            });
        });
    }

    updateLog(dataMap) {
        this.queueManualFunctions.push(() => {
            return new Promise((resolve, reject) => {
                updateLog(dataMap)
                    .then((result) => {
                        //console.log('success update Log', { result });
                        resolve();
                    })
                    .catch((e) => {
                        //console.log('---updateLog error', e);
                        reject();
                    });
            });
        });
    }

    //for synchronization
    queueManualExecute() {
        if (!this.isManualRetrieveProcess) return;
        if (this.queueManualFunctions.length) {
            this.queueManualFunctions[0]()
                .finally(() => {
                    this.queueManualFunctions.shift();
                    this.queueManualExecute();
                });
        } else {
            setTimeout(() => this.queueManualExecute(), 2000);
        }
    }

    handleVFResponse(event) {
        const message = JSON.parse(event.data);
        if (!message) return;
        switch (message.type) {
            case 'trackingManualUpdateLog':
                const {parentId, logFile} = message.data;
                this.updateLog({ dataMap: { parentId, logFile: JSON.stringify(logFile)} });
                break;
            case 'trackingManualFinishRetrieve':
                this.finishManualRetrieve();
                break;

        }
    }

    showToast(message, type) {
        const component = 'c-custom-toast';
        this.template.querySelector(component).showToast(message, type);
    }

    handleRefreshComponents() {
        this.dispatchEvent(new CustomEvent('refresh_components', { detail: { type: 'refreshComponents' } }));
    }
}