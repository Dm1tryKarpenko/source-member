import { LightningElement,api,track} from 'lwc';
import kanbanBoard from '@salesforce/resourceUrl/kanbanBoard';
import { loadStyle } from 'lightning/platformResourceLoader';

export default class OneCardFolder extends LightningElement {

    @api orgData=[];
    @track d=[]
    @api column;
    openAdditionalData=false;
    buttomIcon=kanbanBoard+'/buttomIcon.png';
    buttomGreyIcon=kanbanBoard+'/buttomGreyIcon.png';

    get clockIcon(){
        return kanbanBoard+'/clockIcon.png';
    }

    dateDifference(date){
        var Difference_In_Time = new Date().getTime() - new Date(date).getTime(); 
        return  Math.round(Difference_In_Time / (1000 * 3600 * 24)); 
    }

    openAdditionalDataHandler(event){
        var oneEl = this.d.find(el=> el.row.id===event.currentTarget.dataset.rowid);
        oneEl.row.openAdditionalData=!oneEl.row.openAdditionalData;
    
    }

   parseData(arrayToParse){
        var result =[];
        arrayToParse.map(value=>{
            var row;
            row={ 
                name: value.branchName,
                id:value.id, 
                linkOnBranch: '/'+value.id,
                time: value.lastUpdated, 
                owner: value.owner,
                dateDifference: this.dateDifference(value.lastUpdated),
                openAdditionalData: false,
                dataDifferenceFromDeployment: value.deploymentDate ? this.dateDifference(value.deploymentDate) : null,
                orgName: value.orgName,
                smimLane: value.swimlaneName,
                codeQuality: value.codeReviewState,
                codeQualityIcon :value.codeReviewIcon,
                codeReviewIssueAmount: value.codeReviewIssueAmount,
                impactAnalysis:  value.impactAnalysis!=null ?  value.impactAnalysis.substring(value.impactAnalysis.indexOf('>')+1,
                value.impactAnalysis.lastIndexOf('<'))  : null   
        };
        row={
            ...row,
            isToday: row.dateDifference==0 ? true : false
           }
       result = [ ...result,{row}];
         });
         return result;
    }
    
    connectedCallback() {
        this.d = this.parseData(this.orgData);     
       }

       renderedCallback() {
        loadStyle(this, kanbanBoard+ '/style.css' )
          .then(() => {});
    }

}