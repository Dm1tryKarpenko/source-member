import { LightningElement,api } from 'lwc';
import getDeploymentAnalyzerInfo from '@salesforce/apex/KanbanController.getDeploymentAnalyzerInfo';
import getAmountOfComponents from '@salesforce/apex/KanbanController.getAmountOfComponents';
import getCodeReviewByInfo from '@salesforce/apex/KanbanController.getCodeReviewInfo';
import kanbanBoard from '@salesforce/resourceUrl/kanbanBoard';
import { loadStyle } from 'lightning/platformResourceLoader';

export default class AdditionalDataFolder extends LightningElement {

    @api orgData=[];
    @api lastDeployedDays;
    amountOfComponenets;
    componentsNumberIcon = kanbanBoard+'/componentsNumberIcon.png';
    codeQualityIcon=kanbanBoard+'/codeQualityIcon.png';
    codeReviewIcon=kanbanBoard+'/codeReviewIcon.png';
    analysisIcon=kanbanBoard+'/analysisIcon.png';
    clockIcon=kanbanBoard+'/clockIcon.png';
    style;
    deploymentAnalyzerInfo;
    codeReviewBy;
    codeQualityResult;
    error;
    issuesAmount;

    renderedCallback() {
      loadStyle(this, kanbanBoard + '/style.css')
        .then(() => {});
  }
    getCodeQualityResult(iconText){
      return iconText.substring(iconText.indexOf('alt')+5,iconText.indexOf('style')-2)
    }

    get ifToday(){
      return this.orgData.dataDifferenceFromDeployment==0 ? true : false;
    }

    get showDeploymentDate(){
      return this.orgData.dataDifferenceFromDeployment!=null ? true : false;
    }


    connectedCallback() {
      getDeploymentAnalyzerInfo({ branchId: this.orgData.id }).then(response => {
        if (response!=null) { 
            this.deploymentAnalyzerInfo = JSON.parse(response);
        }
      }).catch(error => {
        console.error(JSON.stringify(error));
        this.error = error;
      });

      getCodeReviewByInfo({ branchId: this.orgData.id }).then(response => {
        if (response!=null) { 
            this.codeReviewBy = response;
        }
      }).catch(error => {
        console.error(JSON.stringify(error));
        this.error = error;
      });
      
      getAmountOfComponents({ branchId: this.orgData.id }).then(response => {
        if (response) { 
            this.amountOfComponenets = response;
        }
      }).catch(error => {
        console.error(JSON.stringify(error));
        this.error = error;
      });


        this.codeQualityResult = this.orgData.codeQualityIcon ? this.getCodeQualityResult(this.orgData.codeQualityIcon): null;
        if(this.codeQualityResult==="Issues found" ){
             this.issuesAmount=this.orgData.codeReviewIssueAmount.substring(this.orgData.codeReviewIssueAmount.lastIndexOf(':')+1).trim()
        }
        if(this.orgData.codeQuality==="COMPLETED" &&  this.codeQualityResult!='Issues found'){
              this.style='green'
              this.codeQualityResult='Completed'
        } else {
            this.style='red'
        }
    }
}