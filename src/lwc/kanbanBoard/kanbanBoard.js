import { LightningElement, track } from 'lwc';
import _getFilteredBranches from '@salesforce/apex/KanbanController.getFilteredBranches';
import heapTrack from '@salesforce/apex/KanbanController.heapTrack';
import kanbanBoard from '@salesforce/resourceUrl/kanbanBoard';
import { loadStyle } from 'lightning/platformResourceLoader';
export default class KanbanBoard extends LightningElement {

    @track columns=[];
    @track rows=[];
    @track error;
    @track value='One month' ;
    @track days =30;

    renderedCallback() {
        loadStyle(this, kanbanBoard + '/style.css')
          .then(() => {});
    }

    get options() {
        return [
            { label: '24 hours', value: '24 hours',days:1 },
            { label: '48 hours', value: '48 hours',days: 2 },
            { label: '72 hours', value: '72 hours' ,days:3},
            { label: 'One week', value: 'One week' ,days:7 },
            { label: '2 weeks', value: '2 weeks' ,days:14},
            { label: 'One month', value: 'One month',days:30 },
        ];
    }

    handleChange(event) {
        this.value = event.detail.value;
        this.options.map(e=>{
            if(e.value===this.value){
                this.days=e.days;
            }
        })
        this.getFilteredData();
        
    }

    getFilteredData(){
        this.columns=[];
        this.rows=[];
        _getFilteredBranches({ daysFilter:  this.days}).then(response => {
            
            if (response) { 
                response.columns.map (element => {
                this.columns = [...this.columns, {
                    type: element.type,
                    amount: element.amount,
                    icon: element.icon = kanbanBoard+'/'+element.icon+'.png'
                    /*'stagingIcon' ? kanbanBoard+'/stagingIcon.png' 
                    : element.icon=='UATIcon' ? kanbanBoard+'/UATIcon.png'  : element.icon=='devIcon'
                     ? kanbanBoard+'/devIcon.png' :  element.icon=='QAIcon' ? kanbanBoard+'/QAIcon.png' 
                     : element.icon=='deploymentIcon' ? kanbanBoard+'/deploymentIcon.png' : null*/
                  }];
                })
      
                for(var key in response.rows){
                    this.rows.push({ key:key,value:response.rows[key]}); 
                }
            }
          }).catch(error => {
            console.error(JSON.stringify(error));
            this.error = error;
          });
        }

    track(){
        heapTrack()
        .then(response => {
            
        })
        .catch(error => {
            console.log(JSON.stringify(error));
        });
    }

    connectedCallback() {
      this.getFilteredData();
      this.track();
  }
}