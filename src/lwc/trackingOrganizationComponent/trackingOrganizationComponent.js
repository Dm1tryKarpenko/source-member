import {api, LightningElement, track, wire} from 'lwc';
import {NavigationMixin} from "lightning/navigation";
import authorize from "@salesforce/apex/TrackingOrganizationController.authorize";
import checkSupportedSourceMember from "@salesforce/apex/TrackingOrganizationController.checkSupportedSourceMember";
import getNamespacePrefix from "@salesforce/apex/TrackingOrganizationController.getNamespacePrefix";
import getRelatedListOrgComponentsId from "@salesforce/apex/TrackingOrganizationController.getRelatedListOrgComponentsId";
import getSettings from "@salesforce/apex/TrackingOrganizationController.getSettings";
import getMetadataTypeList from "@salesforce/apex/TrackingOrganizationController.getMetadataTypeList";
import getSourceMembers from "@salesforce/apex/TrackingOrganizationController.getSourceMembers";
import getLatestComponents from "@salesforce/apex/TrackingOrganizationController.getLatestComponents";
import getUserNames from "@salesforce/apex/TrackingOrganizationController.getUserNames";
import launchRetrieve from "@salesforce/apex/TrackingOrganizationController.launchRetrieve";
import saveSettings from "@salesforce/apex/TrackingOrganizationController.saveSettings";
import checkProcessState from "@salesforce/apex/TrackingOrganizationController.checkProcessState";
import createLog from "@salesforce/apex/TrackingOrganizationController.createLog";
import pullChanges from "@salesforce/apex/TrackingOrganizationController.pullChanges";
import getLogs from "@salesforce/apex/TrackingOrganizationController.getLogs";
import getLogAttachmentBody from "@salesforce/apex/TrackingOrganizationController.getLogAttachmentBody";
import notSupportedTemplate from './notSupportedSourceMember.html';
import supportedTemplate from './trackingOrganizationComponent.html';
import supportedTemplateClassic from './classicSupportedSourceMember.html';
import notSupportedTemplateClassic from './classicNotSupportedSourceMember.html';

import {
  convertToJSON,
  TOAST_TYPE_SUCCESS,
  TOAST_TYPE_ERROR,
  TOAST_TYPE_WARNING,
  DataConverter,
  splitToChunks
} from 'c/trackingOrganizationHelper';

const SORT_BY_STATUS_LIST = ['Too Large','Not Retrieved', 'Deleted', 'Retrieved']; //sorting occurs according to the place in the array
const COMMIT_TO_BRANCH_PAGE = 'OrgComponentToBranchCommit';
const ERROR_MESSAGES = ['STORAGE LIMIT EXCEEDED'];


export default class TrackingOrganizationComponent extends NavigationMixin(LightningElement)  {
  @api recordId;
  @api objectApiName;
  @api isClassic = false;

  @track objectInfo;

  @track namespacePrefix = '';
  @track urlCommitToBranchPage = '';

  @track accessToken = '';
  @track instanceUrl = '';

  @track frequency = '5';

  @track filterComponentType = 'All';
  @track filterComponentSearch = '';

  @track notSupportedTitle = 'Latest Components ';

  @track sortField = '';
  @track sortA = 1;
  @track sortB = -1;
  @track sortByStatusList = [...SORT_BY_STATUS_LIST];

  @track _logList = [];
  @track activeManualLog = null;
  @track isLogList = false;

  @track selectedMetadataTypeList = [];
  @track metadataTypeOptionList = [];
  @track dualListMetadataType = null;

  @track changedComponentList = [];
  @track filteredChangedComponentList = [];
  @track showedChangedComponentList = [];
  @track filteredTooLargeComponentList = [];

  @track filterMetadataTypeOptionList = [{ label: 'All', value: 'All' }];

  @track isSupported = false;
  @track isLoading = true;
  @track isEnabled = true;
  @track isSelectedAll = false;
  @track isShowMore = true;
  @track isNotAbleRetrieve = false;
  @track isChangedComponentListEmpty = true;
  @track isShowCommitToBranchModal = false;
  @track isShowNotRetrievedComponents = false;
  @track isPullManually = false;
  @track isShowModal = false;
  @track isProcessRefreshTableSourceMemberRecords = false;

  @track paginationInfo = {
    recordsPerPage: 10,
    currentPage: 0,
    pagesCount: 0,
    isDisableLeftSection: true,
    isDisableRightSection: false,
    label: ''
  }

  get frequencyOptionList() {
    return [
      { label: 'Off', value: '0'},
      { label: '5 minutes', value: '5'},
      { label: '15 minutes', value: '15' },
      { label: '30 minutes', value: '30' },
      { label: '1 hour', value: '60' },
      { label: '3 hours', value: '180' },
      { label: '6 hours', value: '360' },
      { label: '8 hours', value: '480' },
      { label: '12 hours', value: '720' },
      { label: 'Daily', value: '1440' },
    ];
  }

  get latestComponentsCount() {
    if (this.changedComponentList && this.changedComponentList.length) {
      return this.changedComponentList.length > 6 ? '6+' : this.changedComponentList.length;
    } else {
      return 0;
    }
  }

  get selectedComponentIdList() {
    if (this.changedComponentList && this.changedComponentList.length) {
      return this.changedComponentList.filter((comp) => comp.checked && comp.status === 'Retrieved').map((comp) => comp.id);
    } else {
      return [];
    }
  }

  get selectedDeletedComponentIdList() {
    if (this.changedComponentList && this.changedComponentList.length) {
      return this.changedComponentList.filter((comp) => comp.checked && comp.status === 'Deleted').map((comp) => comp.id);
    } else {
      return [];
    }
  }

  get isExistComponents() {
    return (this.changedComponentList && this.changedComponentList.length);
  }

  get logList() {
    return this._logList;
  }

  set logList(value) {
    this._logList = value;
    this.isLogList = Array.isArray(value) && value.length > 0;
  }

  render() {
    if (this.isClassic) {
      return this.isSupported ? supportedTemplateClassic : notSupportedTemplateClassic;
    } else {
      return this.isSupported ? supportedTemplate : notSupportedTemplate;
    }
  }

  connectedCallback() {
    Promise.resolve()
      .then(() => this.callAuthorize())
      .then(() => this.callNamespacePrefix())
      .then(() => this.callCheckSupportedSourceMember())
      .then(() => {
        if (this.isSupported) {
          return this.supportedAction();
        } else {
          return this.notSupportedAction();
        }
      })
      .then(() => this.isLoading = false)
      .catch((e) => {
        this.isLoading = false
        this.showToast(e, TOAST_TYPE_ERROR);
      })
  }



  supportedAction() {
    return Promise.resolve()
        .then(() => this.callGetMetadataTypeList())
        .then(() => this.callGetSettings())
        .then(() => this.callLaunchRetrieve())
        .then(() => this.callGetLogs())
        .then((logs) => this.setLogs(logs))
        .then(() => this.refreshTableSourceMemberRecords())
        .then(() => this.updateProcessingLogs());
  }

  notSupportedAction() {
    return Promise.resolve()
      .then(() => this.callGetLatestComponents())
  }

  initPagination() {
    return new Promise((resolve, reject) => {
      try {
        const pagination = this.paginationInfo;
        const filterList = this.filteredChangedComponentList;

        pagination.pagesCount =  Math.ceil(filterList.length / pagination.recordsPerPage);
        pagination.currentPage = 0;
        pagination.isDisableLeftSection = true;
        pagination.isDisableRightSection = pagination.pagesCount <= pagination.currentPage + 1;
        this.updateComponentsTable(filterList);
        resolve();
      } catch (e) {
        reject(e);
      }
    });
  }

  updateComponentsTable() {
    const pagination = this.paginationInfo;
    const filterList = this.filteredChangedComponentList;


    const showedComponentList = [];
    for (let i = pagination.currentPage * pagination.recordsPerPage;
         i < (pagination.currentPage * pagination.recordsPerPage) + pagination.recordsPerPage;
         i++
    ) {
      if (filterList[i]) {
        showedComponentList.push(filterList[i]);
      } else {
        break;
      }
    }

    this.showedChangedComponentList = showedComponentList;
    const emptyPaginationLabel = `Total ${filterList.length} changed component(s)`;
    const normalPaginationLabel = emptyPaginationLabel + `, Page ${pagination.currentPage + 1} of ${pagination.pagesCount}`;
    pagination.label = filterList.length > 0
                                          ? normalPaginationLabel
                                          : emptyPaginationLabel;
  }

  callAuthorize() {
    return new Promise((resolve, reject) => {
      try {
        authorize({ dataMap: { organizationId: this.recordId } })
          .then((authorizeDetailsString) => convertToJSON(authorizeDetailsString))
          .then((authorizeDetails) => {
            if (authorizeDetails.isSuccess) {
              this.accessToken = authorizeDetails.accessToken;
              this.instanceUrl = authorizeDetails.instanceUrl;
              resolve();
            } else {
              reject(authorizeDetails.errorMessage);
            }
          })
          .catch((e) => {
            reject('Error while Authorization.');
          })
      } catch (e) {
        reject('Error while Authorization.');
      }
    });
  }

  callNamespacePrefix() {
    getNamespacePrefix()
        .then((result) => {
          if (result && result.length > 0) this.namespacePrefix = result + '__';
          this.urlCommitToBranchPage = '/apex/' + this.namespacePrefix + COMMIT_TO_BRANCH_PAGE;
        });
  }

  callCheckSupportedSourceMember() {
    return new Promise((resolve, reject) => {
      try {
        checkSupportedSourceMember({ dataMap: { accessToken: this.accessToken, instanceUrl: this.instanceUrl } })
          .then((supportedDetailsString) => convertToJSON(supportedDetailsString))
          .then((supportedDetails) => {
            this.isSupported = supportedDetails.isSupported;
            resolve();
          })
          .catch((e) => {
            reject('Error while check \'Source Member\' support.');
          })
      } catch (e) {
        reject('Error while check \'Source Member\' support.');
      }
    });
  }

  callLaunchRetrieve() {
    return new Promise((resolve, reject) => {
      try {
        launchRetrieve({ dataMap: { organizationId: this.recordId } })
            .then(() => resolve())
            .catch((e) => {
              const {body: { message = '' } = {} } = e;
              this.isNotAbleRetrieve = true;
              this.showToast('Error while Launch Retrieve: ' + message, TOAST_TYPE_ERROR);
              resolve();
            });
      } catch (e) {
        reject(e);
      }
    });
  }

  callGetSettings() {
    return new Promise((resolve, reject) => {
      try {
        const metadataTypeList = this.metadataTypeOptionList.map((item) => item.value);
        getSettings({ dataMap: { organizationId: this.recordId, metadataTypeOptionList: JSON.stringify(metadataTypeList), frequency: this.frequency } })
          .then((settingsJson) => convertToJSON(settingsJson))
          .then((settings) => {
            this.selectedMetadataTypeList = settings.metadataTypeList ? settings.metadataTypeList : [];
            this.frequency = settings.frequency ? settings.frequency: '0';
            this.isEnabled = !!settings.isEnabled;
            resolve();
          })
          .catch((e) => {
            reject('Error while get settings.');
          });
      } catch (e) {
        reject('Error while get settings.');
      }
    });
  }

  callGetLatestComponents() {
    return new Promise((resolve, reject) => {
      try {
        getLatestComponents({ dataMap: { organizationId: this.recordId, isAll: !!!this.objectApiName, lastId: '' } })
          .then((latestComponentsJson) => convertToJSON(latestComponentsJson))
          .then((latestComponentList = []) => {
            this.notSupportedTitle += latestComponentList.length > 6 ? `(6+)` : `(${latestComponentList.length})`;
            if (this.isClassic) {
              latestComponentList.splice(16);
            } else {
              latestComponentList.splice(6);
            }
            this.changedComponentList = latestComponentList;
            this.filteredChangedComponentList = latestComponentList;
            this.showedChangedComponentList = latestComponentList;
            resolve();
          })
          .catch((e) => {
            reject('Error while get latest components.');
          })
      } catch (e) {
        reject('Error while get latest components.');
      }
    });
  }

  updateProcessingLogs() {
    const delay = 5000;
    const delayForActivesProcess = 2000;

    const logProcessList = this.logList.filter(item => item.status === 'In Progress');
    const logNotProcessList = this.logList.filter(item => item.status !== 'In Progress');
    if (logProcessList.length > 0) {
      const countOfRecords = 1;
      const logIdAlreadyRetrievedList = logNotProcessList.map(item => item.id);

      Promise.resolve()
          .then(() => this.callGetLogs({ countOfRecords, logIdAlreadyRetrievedList }))
          .then(data => {
            const item = data[0];
            if (item) {
              const indexElementInLogList = this.logList.findIndex(element => element.id === item.id);

              const content = this.template.querySelector(`div[data-id="logs"] div[data-id="${item.contentId}"]`);

              if (indexElementInLogList === 0) {
                if (content) {
                  const logBodyBlock = content.querySelector(`div.log-body-block`);
                  const logBody = this.isClassic ? content.querySelector(`div.log-content-body`) : logBodyBlock;
                  const autoscroll = content.querySelector(`.autoscroll`);
                  item.isAutoscroll = autoscroll ? autoscroll.checked : false;

                  if (logBody && logBodyBlock) {
                    logBodyBlock.innerHTML = item.body;
                    if (item.isAutoscroll) logBody.scrollTop = logBody.scrollHeight;
                  }
                  if (item.isCompleted) {
                    item.isAutoscroll = true;
                    setTimeout(() => {
                      if (!this.isLoading) this.refreshTableSourceMemberRecords();
                    }, 1000);
                  }
                  if (item.status === 'Exception') {
                    if (ERROR_MESSAGES.some((error) => `(${error})` === item.errorMessage)) {
                      this.isNotAbleRetrieve = true;
                      this.showToast('Error occurred while receiving' + item.errorMessage, TOAST_TYPE_ERROR);
                    }
                  }
                }
                const { tabLogClass, contentClass } = this.logList[indexElementInLogList];
                this.logList[indexElementInLogList] = {...item, tabLogClass, contentClass};
                this.activeManualLog = this.logList[indexElementInLogList];

              } else if (indexElementInLogList < 0) {
                this.setLogs([item, ...this.logList]);
              }

              if (indexElementInLogList === 0)  logProcessList.shift();
              logProcessList.forEach((item) => {
                item.status = 'Exception';
                item.icon = 'utility:error';
                item.isInProcess = false;
                item.body += '\nLog is in endless process';
              });
            }
          })
          .then(() => {
            setTimeout(() => this.updateProcessingLogs(), delayForActivesProcess);
          })
          .catch(e => {
            this.showToast('Error while update log', TOAST_TYPE_ERROR);
            console.log('--- Error while update log', e);
          });
    } else {
      const logIdAlreadyRetrievedList = logNotProcessList.map(item => item.id);
      logIdAlreadyRetrievedList.splice(0,1);

      Promise.resolve()
          .then(() => this.callGetLogs({countOfRecords: 1, logIdAlreadyRetrievedList}))
          .then((data = []) => {
            const firstLogId = this.logList[0] ? this.logList[0].id : null;
            if (data[0] && data[0].id !== firstLogId) {
              this.setLogs([data[0],...this.logList]);
              return true;
            }
            return false;
          })
          .then((isNewProcess) => {
            setTimeout(
                () => this.updateProcessingLogs(),
                isNewProcess ? delayForActivesProcess : delay
            );
          })
          .catch((e) => {
            this.showToast('Error while update log', TOAST_TYPE_ERROR);
            console.log('--- Error updateProcessingLogs', e);
          });
    }
  }

  setLogs(logs = []) {
    if (logs.length) {
      logs.forEach((item) => {
        item.tabLogClass = item.tabLogClass.replaceAll('slds-is-active', '');
        item.contentClass = item.contentClass.replaceAll('slds-show','slds-hide');
      });
      logs[0].tabLogClass += ' slds-is-active';
      logs[0].contentClass = logs[0].contentClass.replaceAll('slds-hide', 'slds-show');
    }
    this.activeManualLog = logs[0];
      this.logList = logs;
  }

  callGetLogs(settings = {}) {
    return new Promise((resolve, reject) => {
      try {

        const {
          countOfRecords = 25,
          logIdAlreadyRetrievedList = this.logList.map(item => item.id)
        } = settings;

        this.helpCallGetLogs(countOfRecords, logIdAlreadyRetrievedList)
            .then(data => {
              if (data.length < countOfRecords) this.isShowMore = false;
              resolve(data);
            })
            .catch(reject);

      } catch (e) {
        reject('Error while get logs.');
      }
    });
  }

  callLogBody(logList, processedAttachmentIdList = []) {
    return new Promise((resolve, reject) => {
      try {
        getLogAttachmentBody({ dataMap: {
            logIdList: JSON.stringify(logList.map((log) => log.id)),
            processedAttachmentIdList: JSON.stringify(processedAttachmentIdList)
          }
        })
          .then((result) => {
            Object.keys(result.metadataLogIdToBodiesMap).forEach((metadataLogId) => {
              const log = logList.find((log) => log.id === metadataLogId);
              if (!log.body) {
                log.body = '';
              }
              log.body += result.metadataLogIdToBodiesMap[metadataLogId];
            });
            if (result.processedAttachmentIdList) {
              this.callLogBody(logList, [...processedAttachmentIdList, ...result.processedAttachmentIdList])
                .then(resolve);
            } else {
              resolve(logList);
            }
          })
          .catch((e) => {
            console.log('--- error callLogBody ', e);
            reject(e);
          })
      } catch (e) {
        console.log('--- error callLogBody ', e);
        reject(e);
      }
    });
  }

  helpCallGetLogs(
      countLeft = 25,
      logIdAlreadyRetrievedList = this.logList.map( elem => elem.id),
      recursiveLogList = []
  ) {
    return new Promise((resolve, reject) => {
      try {
        const logIdAlreadyRetrievedListTemporary = recursiveLogList.map( elem => elem.id);

        getLogs({ dataMap: {
            organizationId: this.recordId,
            logIdAlreadyRetrievedList: JSON.stringify([...logIdAlreadyRetrievedList,...logIdAlreadyRetrievedListTemporary]),
            countLeft
        } })
            .then((logsJson) => convertToJSON(logsJson))
            .then((logList) => this.callLogBody(logList))
            .then((logList) => {
              if (Array.isArray(logList) && logList.length > 0) {
                const newCountLeft = countLeft - logList.length;
                logList.forEach((log) => {
                  log.tabLogId = 'tab-log-' + log.id;
                  log.contentId = 'content-' + log.id;
                  log.tabLogClass = 'slds-vertical-tabs__nav-item log-tab';

                  log.contentClass = 'slds-vertical-tabs__content log-content';
                  log.contentClass += ' slds-hide';
                  log.icon = log.status === 'Completed'
                      ? 'utility:success'
                      : log.status === 'Exception'
                          ? 'utility:error'
                          : '';

                  log.isInProcess = log.status === 'In Progress';
                  log.isNotInProcess = !log.isInProcess;
                  log.isAutoscroll = true;
                  log.errorMessage = log.errorMessage && log.errorMessage.length
                        ? `(${log.errorMessage})`
                        : '';
                });

                recursiveLogList.push(...logList);

                if (newCountLeft > 0) {
                  this.helpCallGetLogs(newCountLeft, logIdAlreadyRetrievedList, recursiveLogList)
                      .then(data => resolve(data))
                      .catch(reject);
                } else {
                  resolve(recursiveLogList);
                }
              } else {
                resolve(recursiveLogList);
              }
            })
            .catch((e) => {
              reject('Error while get logs');
            });
      } catch (e) {
        reject('Error while get logs.');
      }
    });
  }

  callGetMetadataTypeList() {
    return new Promise((resolve, reject) => {
      try {
        getMetadataTypeList({ dataMap: { accessToken: this.accessToken, instanceUrl: this.instanceUrl } })
          .then((metadataTypeListString) => convertToJSON(metadataTypeListString))
          .then((metadataTypeList) => {
            return DataConverter.convertToOptionList(metadataTypeList)
          })
          .then((metadataTypeOptionList) => {
            this.metadataTypeOptionList = metadataTypeOptionList;
            if (metadataTypeOptionList.length === 0) {
              this.showToast(
                  'Metadata types not found! You can set it in Flosum > Settings > Retrieve Changes > Snapshot Component Types to Retrieve',
                  TOAST_TYPE_WARNING
              );
            }
          })
          .then(resolve)
          .catch((e) => {
            reject('Error while get metadata type list.');
          });
      } catch (e) {
        reject('Error while get metadata type list.');
      }
    });
  }

  callGetSourceMembers() {
    return new Promise((resolve, reject) => {
      try {

        this.changedComponentList = [];
        this.filteredChangedComponentList = [];
        this.showedChangedComponentList = [];
        let whereClause = '';
        whereClause += '+MemberType+IN+(';

        this.selectedMetadataTypeList.forEach((type) => whereClause += `'${type}',+`);
        whereClause = whereClause.slice(0, -2);
        whereClause += ')';


        let sourceMemberList = [];
        const dataMap = { accessToken: this.accessToken, instanceUrl: this.instanceUrl, whereClause, organizationId: this.recordId  };
        Promise.resolve()
            .then(() => this.selectedMetadataTypeList.length > 0 ? this.helpCallGetSourceMembers(dataMap) : [])
            .then((list) => sourceMemberList = list)
            .then(() => this.helpCallOrgComponents())
            .then(() => {
              if (sourceMemberList.length) {
                const changeDetectedMap = new Map();
                const newChangedComponentList = [];

                sourceMemberList.forEach((item) => {
                  changeDetectedMap.set(`${item.componentType}#${item.componentName}`, item);
                });
                this.changedComponentList.forEach((item) => {
                  const key = `${item.componentType}#${item.componentName}`;
                  if (changeDetectedMap.has(key)) {
                    let value = changeDetectedMap.get(key);
                    changeDetectedMap.delete(key);
                    if (this.isShowNotRetrievedComponents) newChangedComponentList.push(value);
                  } else {
                    newChangedComponentList.push(item);
                  }
                });
                if (this.isShowNotRetrievedComponents) newChangedComponentList.push(...changeDetectedMap.values());
                this.changedComponentList = newChangedComponentList;
              }
            })
            .then(() => {
              this.filteredChangedComponentList = this.changedComponentList;
              this.isChangedComponentListEmpty = this.changedComponentList.length === 0;
              this.isPullManually = this.changedComponentList.some(item => item.status === 'Too Large');
              const componentTypeSet = new Set();
              this.changedComponentList.forEach((sourceMember) => componentTypeSet.add(sourceMember.componentType));
              return Promise.resolve(componentTypeSet);
            })
            .then(() => DataConverter.setSourceMemberAction(this.changedComponentList))
            .then(() => DataConverter.convertToOptionList(new Set(this.changedComponentList.map((c) => c.componentType))))
            .then((componentTypeOptionList) => this.filterMetadataTypeOptionList = [{ label: 'All', value: 'All' }, ...componentTypeOptionList])
            .then(() => {
              //todo delete?
              const childComponent = this.template.querySelector('c-tracking-organization-changed-components')[0];
            })
            .then(resolve)
            .catch((e) => {
              console.log('--- Error callGetSourceMembers', e);
              reject('Error while get \'Source Member\' records.');
            })
      } catch (e) {
        reject('Error while get \'Source Member\' records.');
      }
    });
  }

  helpCallGetSourceMembers(dataMap, recursiveList = []) {
    return new Promise((resolve, reject) => {
      try {
        let nextRecordUrl = '';
        getSourceMembers({ dataMap })
          .then((sourceMemberResponseString) => convertToJSON(sourceMemberResponseString))
          .then((sourceMemberResponse) => {
            recursiveList.push(...sourceMemberResponse.changedList);
            nextRecordUrl = sourceMemberResponse.nextRecordUrl;
            if (nextRecordUrl) {
              dataMap.nextRecordsUrl = nextRecordUrl;
              this.helpCallGetSourceMembers(dataMap, recursiveList)
                .then((result) => resolve(result))
                .catch(reject);
            } else {
              resolve(recursiveList);
            }
          })
          .catch(reject)
      } catch (e) {
        reject(e);
      }
    });
  }

  helpCallOrgComponents(lastId = '') {
    return new Promise((resolve, reject) => {
      try {
        getLatestComponents({ dataMap: { organizationId: this.recordId, isAll: true, isIncludeTooLarge: true, lastId } })
            .then((latestComponentsJson) => convertToJSON(latestComponentsJson))
            .then((latestComponentList = []) => {
              this.changedComponentList.push(...latestComponentList);
              if (latestComponentList.length === 50000) {
                this.helpCallOrgComponents(latestComponentList[latestComponentList.length -1].id)
                    .then(() => resolve());
              } else {
                resolve();
              }
            })
            .catch((e) => {
              reject('Error while get latest components.');
            })
      } catch (e) {
        reject(e);
      }
    });
  }

  callGetGetUserNames() {
    return new Promise((resolve, reject) => {
      try {
        const userIdSet = new Set();
        let userIds = '';
        const changedComponentList = this.changedComponentList.filter((item) => item.status === 'Not Retrieved');
        changedComponentList.forEach((sm) => userIdSet.add(sm.changedById));
        userIdSet.forEach((id) => userIds += `'${id}',`);
        userIds = userIds.slice(0, -1);
        if (userIds) {
          getUserNames({ dataMap: { accessToken: this.accessToken, instanceUrl: this.instanceUrl, userIds } })
            .then((userNamesResponse) => convertToJSON(userNamesResponse))
            .then((userNamesResponse) => {
              if (userNamesResponse && userNamesResponse.records && userNamesResponse.records.length) {
                const userMap = {};
                userNamesResponse.records.forEach((user) => userMap[user.Id.substring(0, 15)] = user.Name);
                changedComponentList.forEach((sm) => sm.changedBy = userMap[sm.changedById]);
              }
              resolve();
            })
            .then(resolve)
            .catch((e) => {
              reject(e);
            });
        } else {
          resolve();
        }
      } catch (e) {
        reject(e);
      }
    });
  }

  callSaveSettings(settings) {
    return new Promise((resolve, reject) => {
      try {
        saveSettings({ dataMap: { organizationId: this.recordId, settings: JSON.stringify(settings) } })
          .then(resolve)
          .catch((e) => {
            reject('Error while save \'Source Member\' settings.');
          });
      } catch (e) {
        reject('Error while save \'Source Member\' settings.');
      }
    });
  }

  callCheckProcessState() {
    return new Promise((resolve, reject) => {
      try {
        checkProcessState({ dataMap: { organizationId: this.recordId } })
          .then(resolve)
          .catch((e) => {
            reject(e.message);
          });
      } catch (e) {
        reject('Error while check process state.');
      }
    });
  }

  callCreateLog() {
    return new Promise((resolve, reject) => {
      try {
        createLog({ dataMap: { organizationId: this.recordId } })
          .then((metadataLogJson) => {
            const metadataLog = JSON.parse(metadataLogJson);
            resolve(metadataLog.id);
          })
          .catch((e) => {
            reject(e);
          });
      } catch (e) {
        reject('Error while check process state.');
      }
    });
  }

  //todo delete?
  callPullChanges(componentList, metadataLogId, isFirst, isLast) {
    return new Promise((resolve, reject) => {
      try {

        const dataMap = {
          organizationId: this.recordId,
          accessToken: this.accessToken,
          instanceUrl: this.instanceUrl,
          componentList: JSON.stringify(componentList),
          metadataLogId,
          isFirst,
          isLast
        };

        pullChanges({ dataMap })
          .then((jobResultJson) => {
            const jobResult = JSON.parse(jobResultJson);
            if (jobResult.isSuccess) {
              resolve();
            } else {
              reject(jobResult.errorMessage);
            }
          })
          .catch((e) => {
            reject(e);
          });
      } catch (e) {
        reject('Error while pull changes.');
      }
    });
  }

  handlePullChanges(event) {
    try {
      const processedList = event.target.dataset.id === 'all'
        ? this.changedComponentList
        : this.changedComponentList.filter((sm) => sm.checked);

      if (processedList.length === 0) {
        return this.showToast('Nothing to pull', TOAST_TYPE_ERROR);
      }

      processedList.sort((a, b) => (a.componentType > b.componentType) ? 1 : (b.componentType > a.componentType) ? -1 : 0);

      Promise.resolve()
        .then(() => this.callCheckProcessState())
        .then(() => this.callCreateLog())
        .then((metadataLogId) => {
          const chunkList = splitToChunks(processedList, 500);
          let chunkCount = 0;
          let promiseChain = Promise.resolve();
          chunkList.forEach((chunkSourceMemberList) => {
            const componentMap = {};
            chunkSourceMemberList.forEach((sm) => {
              if (!componentMap[sm.componentType]) {
                componentMap[sm.componentType] = [sm];
              } else {
                componentMap[sm.componentType].push(sm);
              }
            });
            const componentList = [];
            Object.keys(componentMap).forEach((type) => componentList.push({ type: type, componentList: componentMap[type] }));
            const isFirst = chunkCount === 0;
            const isLast = chunkCount === chunkList.length - 1;
            promiseChain = promiseChain.then(() => this.callPullChanges(componentList, metadataLogId, isFirst, isLast));
            chunkCount++;
          });
        })
        .then(() => {
          this.isLoading = false;
        })
        .catch((e) => {
          this.showToast(e, TOAST_TYPE_ERROR);
        })


    } catch (e) {
      this.showToast(e, TOAST_TYPE_ERROR);
    }
  }

  handleSelectAllComponents(event) {
    try {
      this.filteredChangedComponentList.forEach((c) => c.checked = event.detail.isSelectedAll);
      this.updateComponentsTable();
    } catch (e) {
      this.showToast(e, TOAST_TYPE_ERROR);
    }
  }

  handleSelectComponent(event) {
    try {
      const selectedComponent = this.filteredChangedComponentList.find((c) => c.id === event.detail.id);
      selectedComponent.checked = !selectedComponent.checked;
      this.filteredChangedComponentList = [...this.filteredChangedComponentList];
      this.updateComponentsTable();
    } catch (e) {
      this.showToast(e, TOAST_TYPE_ERROR);
    }
  }

  handleSave() {
    try {
      if (this.isSupported) {
        let successMessage = 'Configuration Saved!';
        if (this.selectedMetadataTypeList.length === 0) {
          this.frequency = '0';
          this.isEnabled = false;
          successMessage = 'Organization Tracking Disabled!';
        } else {
          this.isEnabled = true;
        }
        const settings = {
          frequency : this.frequency,
          metadataTypeList: this.selectedMetadataTypeList,
          isEnabled: this.isEnabled
        }
        this.isLoading = true;
        Promise.resolve()
          .then(() => this.callSaveSettings(settings))
          .then(() => this.refreshTableSourceMemberRecords())
          .then(() => {
            this.showToast(successMessage, TOAST_TYPE_SUCCESS);
            this.isLoading = false;
            return Promise.resolve();
          })
          .catch((e) => {
            this.isLoading = false;
            this.showToast(e, TOAST_TYPE_ERROR);
          });
      } else {
        this.showToast('\'SourceMember\' is not supported on target organization!', TOAST_TYPE_ERROR);
      }
    } catch (e) {
      this.showToast(e, TOAST_TYPE_ERROR);
    }
  }

  handleRefresh() {
      try {
        this.isLoading = true;
        this.isShowNotRetrievedComponents = true;
        Promise.resolve()
          .then(() => this.refreshTableSourceMemberRecords())
          .catch((e) => {
            this.isLoading = false;
            this.showToast(e, TOAST_TYPE_ERROR);
          });
      } catch (e) {
        this.showToast(e, TOAST_TYPE_ERROR);
      }
  }

  handleChangeFilterComponentType(event) {
    try {
      this.filterComponentType = event.detail.value;
      this.filterByType();
      this.filterByName();
      this.initPagination();
      const sortingDirection = this.sortA === 1 ? 'ASC' : 'DESC';
      this.handleSort({ detail: { sortField: this.sortField, sortingDirection } });
    } catch (e) {
      this.showToast(e, TOAST_TYPE_ERROR);
    }
  }

  filterByType() {
    this.filteredChangedComponentList = this.filterComponentType === 'All'
      ? this.changedComponentList
      : this.changedComponentList.filter((c) => c.componentType === this.filterComponentType);
  }

  filterByName() {
    if (this.filterComponentSearch) {
      this.filteredChangedComponentList = this.filteredChangedComponentList.filter((c) => c.componentName.toLowerCase().includes(this.filterComponentSearch.toLowerCase()));
    }
  }

  handleChangeSearchBox(event) {
    try {
      this.filterComponentSearch = this.isClassic ? event.target.value : event.detail.value;
      this.filterByType();
      this.filterByName();
      this.initPagination();
      if (this.filterComponentSearch === '') {
        const sortingDirection = this.sortA === 1 ? 'ASC' : 'DESC';
        this.handleSort({ detail: { sortField: this.sortField, sortingDirection } });
      }
    } catch (e) {
      this.showToast(e, TOAST_TYPE_ERROR);
    }
  }

  handlePagination(event) {
    try {
      const type = event.target.dataset.id;
      const paginationInfo = JSON.parse(JSON.stringify(this.paginationInfo));

      switch (type) {
        case 'next':
          paginationInfo.currentPage++;
          break;
        case 'last':
          paginationInfo.currentPage = paginationInfo.pagesCount - 1;
          break;
        case 'previous':
          paginationInfo.currentPage--;
          break;
        case 'first':
          paginationInfo.currentPage = 0;
          break;
      }

      paginationInfo.isDisableLeftSection = paginationInfo.currentPage === 0;
      paginationInfo.isDisableRightSection = paginationInfo.currentPage === (paginationInfo.pagesCount - 1);


      this.paginationInfo = paginationInfo;

      this.updateComponentsTable();
    } catch (e) {
      this.showToast(e, TOAST_TYPE_ERROR);
    }
  }

  handleChangeMetadataTypes(event) {
    try {
      this.selectedMetadataTypeList = event.detail.value;
    } catch (e) {
      this.showToast(e, TOAST_TYPE_ERROR);
    }
  }

  handleSort(event) {
    const newSortField = event.detail.sortField;
    let sortingDirection = event.detail.sortingDirection;

    if (!sortingDirection) {
      sortingDirection = newSortField !== this.sortField
          ? 'ASC'
          : this.sortA === 1 ? 'DESC' : 'ASC';
    }

    this.sortA = sortingDirection === 'ASC' ? 1 : -1;
    this.sortB = sortingDirection === 'ASC' ? -1 : 1;

    let sortedChangedComponentList = [...this.filteredChangedComponentList];

    if (newSortField === 'changedOn') {
      sortedChangedComponentList.sort((a, b) => (parseInt(a[newSortField].value) > parseInt(b[newSortField].value))
          ? this.sortA
          : ((parseInt(b[newSortField].value) > parseInt(a[newSortField].value)) ? this.sortB : 0));
    } else if(newSortField === 'status') {
      sortedChangedComponentList = this.helpSortStatus(sortedChangedComponentList, sortingDirection);
    } else {
      sortedChangedComponentList.sort((a, b) => {
        const aF = a[newSortField] ? a[newSortField].toLowerCase() : '';
        const bF = b[newSortField] ? b[newSortField].toLowerCase() : '';
        if (aF > bF) return this.sortA;
        if (bF > aF) return this.sortB;
        return 0;
      });
    }
    this.filteredChangedComponentList = sortedChangedComponentList;
    this.sortField = newSortField;


    this.paginationInfo.currentPage = 0;

    this.updateComponentsTable();
  }

  helpSortStatus(sortedChangedComponentList = [], sortingDirection = 'ASC') {
    // sort: Status[DESC Or ASC]. And For every status apply inner sort: Component Name[ASC]
    const statusMap = new Map();
    const newSortedChangedComponentList = [];

    //inner sort: Component Name[ASC]
    sortedChangedComponentList.sort((a, b) =>
        (a.componentName.toLowerCase() > b.componentName.toLowerCase())
            ? 1
            : ((b.componentName.toLowerCase() > a.componentName.toLowerCase())
                ? -1
                : 0)
    );

    sortedChangedComponentList.forEach((item) => {
      if(statusMap.has(item.status)) {
        statusMap.get(item.status).push(item);
      } else {
        statusMap.set(item.status, [item]);
      }
    });

    //main sort
    if (sortingDirection === 'ASC') {
      this.sortByStatusList = [...SORT_BY_STATUS_LIST];
    } else {
      this.sortByStatusList.reverse();
    }
    this.sortByStatusList.forEach((item) => {
      if (statusMap.has(item)) newSortedChangedComponentList.push(...statusMap.get(item));
    });

    return newSortedChangedComponentList;
  }

  //todo delete?
  handleOpenFullScreen() {
    try {
      let componentDef = {
        componentDef: "c:trackingOrganizationComponent",
        attributes: {
          label: 'Navigated ',
          recordId: this.recordId
        }
      };
      let encodedComponentDef = btoa(JSON.stringify(componentDef));
      this[NavigationMixin.GenerateUrl]({
        type: 'standard__webPage',
        attributes: {
          url: '/one/one.app#' + encodedComponentDef
        }
      })
        .then((url) => window.open(url, '_blank'))
        .catch((e) => {
          this.showToast(e, TOAST_TYPE_ERROR);
        })
    } catch (e) {
      this.showToast(e, TOAST_TYPE_ERROR);
    }
  }

  handleChangeFrequency(event) {
    try {
      this.frequency = event.detail.value;
    } catch (e) {
      this.showToast(e, TOAST_TYPE_ERROR);
    }
  }

  handleTabSelected(event) {
    const selectedTab = event.target.closest(".slds-vertical-tabs__nav-item");
    const logId = selectedTab.dataset.id.replace('tab-log-','');

    if (logId !== 'logs-show-more') {
      this.switchLog(logId);
    } else {
      this.handleShowMore();
    }
  }

  switchLog(logId) {
    try {
      const selectedTab = Array.from(this.template.querySelectorAll(`li[data-id=${'tab-log-' + logId}]`));
      const selectedContent = Array.from(this.template.querySelectorAll(`div[data-id=${'content-' + logId}]`));

      const activityTabList = Array.from(this.template.querySelectorAll('li.log-tab.slds-is-active'));
      const activityContentList = Array.from(this.template.querySelectorAll('div.log-content.slds-show'));

      activityTabList.forEach( (elem) => elem.classList.remove('slds-is-active'));
      activityContentList.forEach( (elem) => {
        elem.classList.remove('slds-show');
        elem.classList.add('slds-hide');
      });

      selectedTab.forEach((elem) => elem.classList.add('slds-is-active'));
      selectedContent.forEach((elem) => {
        elem.classList.remove('slds-hide');
        elem.classList.add('slds-show');
      });

    } catch (e) {
      console.log('--- Erorr switchLog', e);
    }
  }

  handleShowMore(event) {
    this.isLoading = true;
    Promise.resolve()
        .then(() => this.callGetLogs())
        .then((logs) => this.logList.push(...logs))
        .then(() => this.isLoading = false)
        .catch(e => {
          this.isLoading = false;
          this.showToast(e, TOAST_TYPE_ERROR);
        });
  }

  handleCommitToBranch(event) {
    const selectedComponentIdAllStatus = this.changedComponentList.filter((comp) => comp.checked);
    if (selectedComponentIdAllStatus && selectedComponentIdAllStatus.length) {
      let message = 'You cannot commit ';

      const typesSet = new Set();
      const cannotCommitStatusList = new Set(['Too Large', 'Not Retrieved']);

      selectedComponentIdAllStatus.forEach((item) => {
        if (cannotCommitStatusList.has(item.status)) {
          typesSet.add(item.statusLabel);
        }
      });

      message += Array.from(typesSet).join(', ') + ' components';

      const isContainCommitComponent = selectedComponentIdAllStatus.some((item) => ['Retrieved','Deleted'].includes(item.status));
      if (isContainCommitComponent) {
        this.isShowCommitToBranchModal = true;
      }

      if (typesSet.size) {
        this.showToast(message, TOAST_TYPE_WARNING);
      }
    } else {
      this.showToast(`Please select least one component.`, TOAST_TYPE_WARNING);
    }
  }

  handleViewAllComponents(event) {
    if (this.isClassic) {
      getRelatedListOrgComponentsId()
          .then((relatedListId = '') => {
            const url = window.location.origin + `/a0Z?rlid=${relatedListId}&id=${this.recordId}`;
            window.open(url, '_blank');
          });
    } else {
      this[NavigationMixin.Navigate]({
        type: 'standard__recordRelationshipPage',
        attributes: {
          recordId: this.recordId,
          objectApiName: 'Organisation__c',
          relationshipApiName: 'Org_Components__r',
          actionName: 'view'
        },
      });
    }
  }

  handleCommitToBranchNotSupported() {
    if (this.isClassic) {
      window.open(window.location.origin + `${this.urlCommitToBranchPage}?id=${this.recordId}`, '_blank');
    } else {
      this[NavigationMixin.Navigate]({
        type: 'standard__webPage',
        attributes: {
          url: `/apex/${pageName}?id=${this.recordId}`
        }
      });
    }
  }

  handleClassicHorizontalTabSelected(event) {
    const selectedTab = event.target.closest('li.tabs_default__item');
    if (!selectedTab || !selectedTab.classList.contains('tabs_default__item')) return;
    const selectedTabId = selectedTab.dataset.id;
    const selectedContent = this.template.querySelector(`div[data-id=${selectedTabId+'-content'}]`);

    const activityTabList = Array.from(this.template.querySelectorAll('li.tabs_default__item.general-tab.tabs_default__item_active'));
    const activityContentList = Array.from(this.template.querySelectorAll('div.slds-tabs_default__content.general-tab.slds-show'));

    activityTabList.forEach( (elem) => elem.classList.remove('tabs_default__item_active'));
    activityContentList.forEach( (elem) => {
      elem.classList.remove('slds-show');
      elem.classList.add('slds-hide');
    });

    selectedTab.classList.add('tabs_default__item_active');
    selectedContent.classList.remove('slds-hide');
    selectedContent.classList.add('slds-show');
  }

  handleCloseCommitToBranchModal() {
    this.isShowCommitToBranchModal = false;
  }

  //Manual Retrieve
  handleToggleModal(event = {}) {
    this.isShowModal = !this.isShowModal;
    if (this.isShowModal) {
      const tooLargeComponents = this.filteredChangedComponentList.filter((item) => item.status === 'Too Large');
      this.filteredTooLargeComponentList = tooLargeComponents.map((item) => ({...item, changedOn: item.changedOn.label}));
    } else {
      const endMessage = event.detail && event.detail.endMessage ? event.detail.endMessage : null;
      if (endMessage) {
        this.showToast(endMessage.message, endMessage.type);
      }
    }
  }

  refreshTableSourceMemberRecords() {
    if (this.isProcessRefreshTableSourceMemberRecords) return;
    else this.isProcessRefreshTableSourceMemberRecords = true;
    this.sortField = '';
    this.isLoading = true;
    return Promise.resolve()
      .then(() => this.callGetSourceMembers())
      .then(() => this.callGetGetUserNames())
      .then(() => this.initPagination())
      .then(() => this.handleSort({ detail: { sortField: 'status' } }))
      .then(() => {
        this.isSelectedAll = false;
        this.filterComponentSearch = '';
        this.filterComponentType = 'All';
        this.filterComponentType2 = 'All';
        this.isLoading = false;
        this.isProcessRefreshTableSourceMemberRecords = false;
        return Promise.resolve();
      })
        .catch((e) => {
          this.showToast('Error while refresh table with source member records', TOAST_TYPE_ERROR);
          console.log('--- Error refreshTableSourceMemberRecords', e);
        });
  }

  showToast(message, type) {
    const component = this.isClassic ? 'c-custom-toast-classic' : 'c-custom-toast';
    this.template.querySelector(component).showToast(message, type);
  }
}
