import {api, track, LightningElement} from 'lwc';

import {
  TOAST_TYPE_SUCCESS,
  TOAST_TYPE_ERROR,
  TOAST_TYPE_WARNING,
} from 'c/trackingOrganizationHelper';

export default class CustomToast extends LightningElement {

  @track isSuccess = false;
  @track isWarning = false;
  @track isError = false;
  @track isShow = false;
  @track message = '';

  @api
  showToast(message, type) {
    if (!this.isShow) {
      this.message = message;
      this.isSuccess = type === TOAST_TYPE_SUCCESS;
      this.isWarning = type === TOAST_TYPE_WARNING;
      this.isError = type === TOAST_TYPE_ERROR;

      if (this.isSuccess || this.isWarning || this.isError) {
        this.isShow = true;
        const toastDiv = this.template.querySelector('[data-id="toast"]');
        toastDiv.classList.add('show');
        toastDiv.classList.add(type);

        setTimeout(() => {
          toastDiv.classList.remove('show');
          toastDiv.classList.remove(type);
          this.isShow = false;
          this.message = '';
        }, 3800);
      }
    }
  }
}