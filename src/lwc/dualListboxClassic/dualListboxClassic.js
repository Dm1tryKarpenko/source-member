/**
 * Created by vladislav on 6.09.21.
 */

import {LightningElement, api} from 'lwc';

export default class DualListboxClassic extends LightningElement {
    @api label = false;
    @api sourceLabel = 'Available';
    @api selectedLabel = 'Selected';

    _options = [];
    _value = [];

    availableList = [];
    selectedList = [];


    @api
    get value() {
        return this._value;
    }

    set value(val) {
        if (Array.isArray(val)) {
            this._value = [...val].sort((a,b) => (''+a).localeCompare(''+b));
            this.updateLists();
        }
    }

    @api
    get options() {
        return this._options;
    }

    set options(val) {
        if (Array.isArray(val)) {
            this._options = [...val].sort((a,b) => (''+a.value).localeCompare(''+b.value));
            this.updateLists();
        }
    }

    updateLists() {
        const filterAvailable = this.options.filter((item) => !this.value.includes(item.value));
        const filterSelected = this.options.filter((item) => this.value.includes(item.value));
        this.availableList = filterAvailable;
        this.selectedList = filterSelected;
    }


    handleChange(event) {
        const target = event.target.closest('div > input[type="button"]');
        if (!target) return;

        const container = this.template.querySelector('[data-id="dual-listbox-metadata-types"]');

        const availableElement = container.querySelector('[data-id="dual-listbox-list-available"]');
        const selectedElement = container.querySelector('[data-id="dual-listbox-list-selected"]');

        const availableList = [...availableElement.selectedOptions].map(option => option.value);
        const selectedList = [...selectedElement.selectedOptions].map(option => option.value);

        const command = target.dataset.id;
        switch (command) {
            case 'all-to-selected':
                this.value = [...this.options.map((item) => item.value)];
                break;
            case 'to-selected':
                this.value = [...availableList, ...this.value];
                break;
            case 'to-available':
                this.value = this.value.filter((item) => !selectedList.includes(item));
                break;
            case 'all-to-available':
                this.value = [];
                break;
        }
        this.dispatchEvent(new CustomEvent('change', {detail: {value: this.value}}));
    }
}