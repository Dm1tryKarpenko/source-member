import {api, LightningElement, track} from 'lwc';

// Example Table Settings
const TABLE_SETTINGS_EXAMPLE = {
  // is include input checkbox on left
  isSupportSelect: true,
  defaultSortField: 'componentName',

  // Pagination Settings
  recordsPerPage: 10,
  // Name of component on pagination: "Total 2138 changed ${paginationLabel}(s), Page 1 of 214"
  paginationLabel: 'component',
  // Sum all sizes from columns should be 12
  columnList: [
    {
      // Label that will show header table
      label: 'Component Name',
      // Filed of component
      field: 'componentName',
      // Is support sorting for this field
      isSorted: true,
      // Column size (size from 12)
      size: 4,
      // type of component field, required for sorting
      type: 'string'
    },
    {
      label: 'Component Type',
      field: 'componentType',
      isSorted: true,
      size: 3,
      type: 'string'
    },
    {
      label: 'Changed By',
      field: 'changedBy',
      isSorted: true,
      size: 2,
      type: 'string'
    },
    {
      label: 'Changed On',
      field: 'changedOn',
      isSorted: true,
      size: 3,
      type: 'date'
    }
  ],
  componentList:[
    {
      // required filed for component
      id: 'id'
    }
  ]
}

export default class ComponentTable extends LightningElement {

  @track sortA = 1;
  @track sortB = -1

  @track privateSortField = '';

  @track privateTableSettings = {};
  @track showedTableRowList = [];
  @track selectedComponentList = [];
  @track selectedComponentIdSet = new Set();

  @track paginationInfo = {
    recordsPerPage: 10,
    currentPage: 0,
    pagesCount: 0,
    isDisableLeftSection: true,
    isDisableRightSection: false,
    label: ''
  }

  isInitializedSort = false;

  @api
  get tableSettings() {
    return this.privateTableSettings;
  }

  set tableSettings(tableSettings) {
    const columnList = tableSettings.columnList;
    const componentList = tableSettings.componentList;
    this.privateSortField = tableSettings.defaultSortField;

    if (columnList && componentList) {
      this.privateTableSettings = JSON.parse(JSON.stringify(tableSettings));
      if (this.privateSortField) {
        this.isInitializedSort = false;
        this.privateTableSettings.componentList.sort((a, b) =>
          (a[this.privateSortField].toLowerCase() > b[this.privateSortField].toLowerCase())
            ? this.sortA
            : ((b[this.privateSortField].toLowerCase() > a[this.privateSortField].toLowerCase())
            ? this.sortB
            : 0)
        );
      }
    }

    if (tableSettings.recordsPerPage) {
      this.paginationInfo.recordsPerPage = tableSettings.recordsPerPage;
      this.initPagination();
    }
  }

  @api
  getSelectedComponentIds() {
    return Array.from(this.selectedComponentIdSet);
  }

  @api
  getSelectedComponents() {
    return this.selectedComponentList;
  }

  renderedCallback() {
    const sortSpan = this.template.querySelector(`[data-sort-field-down="${this.privateSortField}"]`);
    if (!this.isInitializedSort && sortSpan) {
      sortSpan.classList.remove('slds-hide');
      this.isInitializedSort = true;
    }
  }

  get isInitialized() {
    return !!(this.privateTableSettings
      && this.privateTableSettings.columnList
      && this.privateTableSettings.columnList.length
      && this.privateTableSettings.componentList
      && this.privateTableSettings.componentList.length);
  }

  generateTable(columnList, componentList) {
    const tableComponentList = [];
    componentList.forEach((component) => {
      const isChecked = !!component.isChecked;
      const row = { key: `row-${component.id}`, id: component.id, columnList: [], isChecked };
      columnList.forEach((column) => {
        let field = '';
        let isAction = false;
        let isCheckbox = false;
        if (column.type === 'action' || (component[column.field] !== null && component[column.field] !== undefined)) {
          switch (column.type) {
            case 'date':
              field = new Date(component[column.field]).toLocaleString();
            break;
            case 'action':
              field = component[column.field];
              isAction = field && field.length;
            break;
            case 'boolean':
              field = component[column.field];
              isCheckbox = true;
            break;
            default:
              field = component[column.field];
            break;
          }
          row.columnList.push({
            key: `column-${component.id}-${component[column.field]}`,
            field,
            fieldName: column.field,
            size: column.size,
            isAction,
            isCheckbox,
            style: column.style ? column.style : ''
          });
        }
      });
      tableComponentList.push(row);
    });
    return tableComponentList;
  }

  initPagination() {
    const paginationInfo = JSON.parse(JSON.stringify(this.paginationInfo));
    paginationInfo.pagesCount =  Math.ceil(this.privateTableSettings.componentList.length / paginationInfo.recordsPerPage);
    paginationInfo.currentPage = 0;
    paginationInfo.isDisableLeftSection = true;
    paginationInfo.isDisableRightSection = paginationInfo.pagesCount <= paginationInfo.currentPage + 1;
    this.updateRowTable(paginationInfo);
  }

  updateRowTable(paginationInfo) {
    const allComponentList = this.privateTableSettings.componentList;
    const showedComponentList = [];
    for (let i = paginationInfo.currentPage * paginationInfo.recordsPerPage;
         i < (paginationInfo.currentPage * paginationInfo.recordsPerPage) + paginationInfo.recordsPerPage;
         i++
    ) {
      if (allComponentList[i]) {
        showedComponentList.push(allComponentList[i]);
      } else {
        break;
      }
    }

    this.showedTableRowList = this.generateTable(this.privateTableSettings.columnList, showedComponentList);

    const emptyPaginationLabel = `Total ${this.privateTableSettings.componentList.length} ${this.privateTableSettings.paginationLabel}(s)`;
    const normalPaginationLabel = emptyPaginationLabel + `, Page ${paginationInfo.currentPage + 1} of ${paginationInfo.pagesCount}`;
    paginationInfo.label = this.privateTableSettings.componentList.length > 0
      ? normalPaginationLabel
      : emptyPaginationLabel;

    this.paginationInfo = paginationInfo;
  }

  handleSelectComponent(event) {
    const componentId = event.target.dataset.id;
    const selectedComponent = this.privateTableSettings.componentList.find((component) => component.id === componentId);
    if (this.selectedComponentIdSet.has(componentId)) {
      this.selectedComponentList = this.selectedComponentList.filter((component) => component.id !== componentId);
      this.selectedComponentIdSet.delete(componentId);
      selectedComponent.isChecked = false;
    } else {
      this.selectedComponentList.push(selectedComponent);
      this.selectedComponentIdSet.add(componentId);
      selectedComponent.isChecked = true;
    }
    this.handleSelect();
  }

  handleCheckField(event) {
    const isChecked = event.target.checked;
    const componentId = event.target.dataset.id;
    const fieldName = event.target.dataset.field;
    const selectedComponent = this.privateTableSettings.componentList.find((component) => component.id === componentId);
    selectedComponent[fieldName] = isChecked;
  }

  handleSelectAll(event) {
    const isAdd = event.target.checked;
    this.privateTableSettings.componentList.forEach((component) => {
      component.isChecked = isAdd;
      if (isAdd) {
        if (!this.selectedComponentIdSet.has(component.id)) {
          this.selectedComponentIdSet.add(component.id);
          this.selectedComponentList.push(component);
        }
      } else {
        this.selectedComponentList = [];
        this.selectedComponentIdSet.clear();
      }
    });
    this.updateRowTable(this.paginationInfo);
    this.handleSelect();
  }

  handleSort(event) {
    const target = event.target.closest('a');
    const newSortField = target.dataset.id;
    const fieldType = target.dataset.type;


    let sortedRowList = this.privateTableSettings.componentList;
    if (newSortField !== this.privateSortField) {
      this.sortA = 1;
      this.sortB = -1;
    } else {
      this.sortA *= -1;
      this.sortB *= -1;
    }
    if (fieldType === 'date') {
      sortedRowList.sort((a, b) => (a[newSortField] > b[newSortField]) ? this.sortA : ((b[newSortField] > a[newSortField]) ? this.sortB : 0));
    } else {
      sortedRowList.sort((a, b) =>
        (a[newSortField].toLowerCase() > b[newSortField].toLowerCase())
          ? this.sortA
          : ((b[newSortField].toLowerCase() > a[newSortField].toLowerCase())
          ? this.sortB
          : 0)
      );
    }

    const allSortedColumnArrowList = Array.from(this.template.querySelectorAll('.sorted-column-arrow'));
    allSortedColumnArrowList.forEach((item) => item.classList.add('slds-hide'));

    // this.privateTableSettings.columnList.forEach((column) => {
    //   this.template.querySelector(`[data-sort-field-up="${column.field}"]`).classList.add('slds-hide');
    //   this.template.querySelector(`[data-sort-field-down="${column.field}"]`).classList.add('slds-hide');
    // });

    this.sortA === 1
      ? this.template.querySelector(`[data-sort-field-down="${newSortField}"]`).classList.remove('slds-hide')
      : this.template.querySelector(`[data-sort-field-up="${newSortField}"]`).classList.remove('slds-hide');
    this.privateSortField = newSortField;


    this.generateTable(this.privateTableSettings.columnList, sortedRowList);
    this.initPagination();
  }

  handlePagination(event) {
    try {
      const type = event.target.dataset.id;
      const paginationInfo = JSON.parse(JSON.stringify(this.paginationInfo));

      switch (type) {
        case 'next':
          paginationInfo.currentPage++;
          break;
        case 'last':
          paginationInfo.currentPage = paginationInfo.pagesCount - 1;
          break;
        case 'previous':
          paginationInfo.currentPage--;
          break;
        case 'first':
          paginationInfo.currentPage = 0;
          break;
      }

      paginationInfo.isDisableLeftSection = paginationInfo.currentPage === 0;
      paginationInfo.isDisableRightSection = paginationInfo.currentPage === (paginationInfo.pagesCount - 1);
      this.updateRowTable(paginationInfo);
    } catch (e) {
      console.log('--- error handlePagination', e)
    }
  }

  handleAction(event) {
    this.dispatchEvent(new CustomEvent('action', { detail: { id: event.target.dataset.id } }));
  }

  handleSelect() {
    this.dispatchEvent(new CustomEvent('select', { detail: { selectedIdList: this.getSelectedComponentIds() } }));
  }

}
