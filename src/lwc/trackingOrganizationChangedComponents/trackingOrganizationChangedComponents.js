import {api, track, LightningElement} from 'lwc';
import notSupportedTemplate from './notSupportedSourceMember.html';
import supportedTemplate from './trackingOrganizationChangedComponents.html';
import supportedTemplateClassic from './classicSupportedSourceMember.html';
import notSupportedTemplateClassic from './classicNotSupportedSourceMember.html';
import {NavigationMixin} from "lightning/navigation";

import {
  TABLE_COLUMN_LIST
} from 'c/trackingOrganizationHelper';

export default class TrackingOrganizationChangedComponents extends NavigationMixin(LightningElement) {

  @track allChangedComponentList = [];
  @track privateChangedComponentList = [];

  @track privateFilterComponentType = '';
  @track privateFilterComponentSearch = '';

  @track sortA = 1;
  @track sortB = -1;

  @api isSelectedAll = false;
  @api isSupported = false;
  @api isClassic = false;
  @track privateSortField = '';


  render() {
    if (this.isClassic) {
      return this.isSupported ? supportedTemplateClassic : notSupportedTemplateClassic;
    } else {
      return this.isSupported ? supportedTemplate : notSupportedTemplate;
    }
  }

  @api
  get sortField() {
    return this.privateSortField;
  }

  set sortField(newSortField) {
    try {
      if (newSortField) {
        this.privateSortField = newSortField;
        this.showSortArrowField();
      }
    } catch (e) {
      console.error(e);
    }
  }

  @api
  get changedComponentList() {
    return this.privateChangedComponentList;
  }

  set changedComponentList(value) {
    try {
      if (value) {
        const changedComponentList = JSON.parse(JSON.stringify(value));
        if (value.length) {
          changedComponentList[changedComponentList.length - 1].isLast = true;
        }
        this.allChangedComponentList = changedComponentList;
        this.privateChangedComponentList = changedComponentList;
      }
    } catch (e) {
      console.error(e);
    }
  }

  @api
  get filterComponentType() {
    return this.privateFilterComponentType;
  }

  set filterComponentType(value) {
    try {
      this.privateFilterComponentType = value;
      if (this.privateFilterComponentType === 'All') {
        this.privateChangedComponentList = this.allChangedComponentList;
      } else {
        let filterChangedComponentList = JSON.parse(JSON.stringify(this.allChangedComponentList));
        filterChangedComponentList = filterChangedComponentList.filter((component) => component.componentType === this.privateFilterComponentType);
        this.privateChangedComponentList = filterChangedComponentList;
      }
    } catch (e) {
      console.error(e);
    }
  }


  @api
  get filterComponentSearch() {
    return this.privateFilterComponentSearch;
  }

  set filterComponentSearch(value) {
    try {
      this.privateFilterComponentSearch = value;
      if (this.privateFilterComponentSearch) {
        let filterChangedComponentList = JSON.parse(JSON.stringify(this.allChangedComponentList));
        filterChangedComponentList = filterChangedComponentList.filter((component) => component.componentName.toLowerCase().includes(this.privateFilterComponentSearch));
        this.privateChangedComponentList = filterChangedComponentList;
      } else {
        this.privateChangedComponentList = this.allChangedComponentList;
      }
    } catch (e) {
      console.error(e);
    }
  }

  showSortArrowField(timer = 1000) {
    const newSortFieldElement = this.template.querySelector(`[data-id="${this.sortField}"]`);
    if (newSortFieldElement) {
      TABLE_COLUMN_LIST.forEach((column) => {
        this.template.querySelector(`[data-id="source-table-${column}-up"]`).classList.add('slds-hide');
        this.template.querySelector(`[data-id="source-table-${column}-down"]`).classList.add('slds-hide');
      });

      this.sortA === 1
          ? this.template.querySelector(`[data-id="source-table-${this.sortField}-down"]`).classList.remove('slds-hide')
          : this.template.querySelector(`[data-id="source-table-${this.sortField}-up"]`).classList.remove('slds-hide');
    } else {
      if (timer > 0) setTimeout(() => this.showSortArrowField(timer - 10), 10);
    }
  }

  handleSelectComponent(event) {
    try {
      this.dispatchEvent(new CustomEvent('select_component', { detail: { id: event.target.dataset.id } }));
    } catch (e) {
      console.error(e);
    }
  }

  handleSelectAll() {
    try {
      this.isSelectedAll = !this.isSelectedAll;
      this.dispatchEvent(new CustomEvent('select_all', { detail: { isSelectedAll: this.isSelectedAll } }));
    } catch (e) {
      console.error(e);
    }
  }

  handleSort(event) {
    const newSortField = event.target.closest('a').dataset.id;
    this.dispatchEvent(new CustomEvent('sort', { detail: { sortField: newSortField } }));

    TABLE_COLUMN_LIST.forEach((column) => {
      this.template.querySelector(`[data-id="source-table-${column}-up"]`).classList.add('slds-hide');
      this.template.querySelector(`[data-id="source-table-${column}-down"]`).classList.add('slds-hide');
    });

    if (newSortField !== this.sortField) {
      this.sortA = 1;
      this.sortB = -1;
    } else {
      this.sortA *= -1;
      this.sortB *= -1;
    }
    this.sortA === 1
      ? this.template.querySelector(`[data-id="source-table-${newSortField}-down"]`).classList.remove('slds-hide')
      : this.template.querySelector(`[data-id="source-table-${newSortField}-up"]`).classList.remove('slds-hide');

    this.sortField = newSortField;
  }

  handleOpenRecord(event) {
    if (this.isClassic) {
      window.open(window.location.origin+`/${event.target.dataset.id}`,'_blank');
    } else {
      this[NavigationMixin.GenerateUrl]({
        type: 'standard__recordPage',
        attributes: {
          recordId: event.target.dataset.id,
          actionName: 'view',
        },
      }).then((url) => window.open(url, '_blank'));
    }
  }

  handleViewAllComponents(event) {
    this.dispatchEvent(new CustomEvent('view_all_components', { detail: { } }));
  }
}