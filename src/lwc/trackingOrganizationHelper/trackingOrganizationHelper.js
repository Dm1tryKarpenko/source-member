import { ShowToastEvent } from "lightning/platformShowToastEvent";

const TOAST_TYPE_SUCCESS = 'success';
const TOAST_TYPE_WARNING = 'warning';
const TOAST_TYPE_ERROR   = 'error';

const TABLE_COLUMN_LIST = ['componentName', 'componentType', 'changedAction', 'changedBy', 'changedOn', 'status'];

function getCrcWithData(zipData, flags) {
  const r = {};
  if (zipData !== undefined && flags !== undefined && flags.isInnerType === true){
    zipData = Beautify.xml(zipData);
  }
  r.crc32 = normalZip.crc32(zipData,32);
  if (flags !== undefined && flags.compress === true){
    r.data = pako.deflate(zipData, { to: 'string' });
  }else{
    r.data = zipData;
  }
  return r;
}

const convertToJSON = (data) => {
  return new Promise((resolve, reject) => {
    try {
      if (data) {
        resolve(JSON.parse(data));
      } else {
        resolve();
      }
    } catch (e) {
      console.log('--- convertToJSON error', e);
      reject('Error on convert to JSON');
    }
  });
}

const splitToChunks = (array, len) => {
  let chunks = [],
    i = 0,
    n = array.length;

  while (i < n) {
    chunks.push(array.slice(i, i += len));
  }
  return chunks;
}

class DataConverterService {
  convertToOptionList(metadataTypeSet) {
    return new Promise((resolve, reject) => {
      try {
        const metadataTypeOptionList = [];
        metadataTypeSet.forEach((metadataType) => metadataTypeOptionList.push({ label: metadataType, value:metadataType }));
        metadataTypeOptionList.sort((a, b) => (a.label > b.label) ? 1 : ((b.label > a.label) ? -1 : 0));
        resolve(metadataTypeOptionList);
      } catch (e) {
        console.log('--- convertToMetadataTypeOptionList error', e);
        reject(e);
      }
    });
  }

  setSourceMemberAction(sourceMemberList = []) {
    return new Promise((resolve, reject) => {
      try {
        sourceMemberList.forEach((record) => {
          let action = 'Changed';
          if (record.isDeleted) {
            action = 'Deleted';
          }
          if (record.isNew) {
            action = 'New';
          }
          record.changedAction = action;
        });
        resolve();
      } catch (e) {
        console.log('--- convertToSourceMemberList error', e);
        reject(e);
      }
    });
  }
}

class ToastService {
  template;
  constructor(template) {
    this.template = template;
  }

  getWarningToast(message) {
    return new ShowToastEvent({
      title: '',
      message: message,
      variant: 'warning',
      mode: 'pester'
    });
  }

  getErrorToast(message) {
    return new ShowToastEvent({
      title: '',
      message: message,
      variant: 'error',
      mode: 'pester'
    });
  }
}

const DataConverter = new DataConverterService();

export {
  TABLE_COLUMN_LIST,
  TOAST_TYPE_SUCCESS,
  TOAST_TYPE_ERROR,
  TOAST_TYPE_WARNING,
  ToastService,
  DataConverter,
  convertToJSON,
  splitToChunks
}
