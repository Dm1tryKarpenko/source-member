import {api, track, LightningElement} from 'lwc';

import {
  TOAST_TYPE_SUCCESS,
  TOAST_TYPE_ERROR,
  TOAST_TYPE_WARNING,
  DataConverter,
} from 'c/trackingOrganizationHelper';

export default class CustomToastClassic extends LightningElement {

  @track isSuccess = false;
  @track isWarning = false;
  @track isError = false;
  @track isShow = false;
  @track message = '';

  @api
  showToast(message, type, timer = 4000) {
    if (!this.isShow) {
      this.message = message;
      this.isSuccess = type === TOAST_TYPE_SUCCESS;
      this.isWarning = type === TOAST_TYPE_WARNING;
      this.isError = type === TOAST_TYPE_ERROR;

      const showClass = timer === 4000 ? 'show' : 'show-animation_none';

      if (this.isSuccess || this.isWarning || this.isError) {
        this.isShow = true;
        const toastDiv = this.template.querySelector('[data-id="toast"]');
        toastDiv.classList.add(showClass);
        toastDiv.classList.add(type);

        if (timer > 0) {
          setTimeout(() => {
            toastDiv.classList.remove(showClass);
            toastDiv.classList.remove(type);
            this.isShow = false;
            this.message = '';
          }, timer);
        }

      }
    }
  }
}