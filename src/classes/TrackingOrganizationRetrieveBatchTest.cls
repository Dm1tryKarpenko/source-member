@IsTest
private class TrackingOrganizationRetrieveBatchTest {
    @TestSetup
    public static void setup() {
        Organisation__c org = new Organisation__c(
            Username__c = 'test@test.com',
            User_Full_Name__c = 'test person',
            OwnerId = UserInfo.getUserId()
        );
        insert org;

        Tracking_Setting__c trackingOrganizationSetting = new Tracking_Setting__c();
        trackingOrganizationSetting.Organization__c = org.Id;
        trackingOrganizationSetting.Current_Jobs__c = 'TestId#Profile';
        trackingOrganizationSetting.Is_Processed__c = true;
        DatabaseUtils.insertRecord(
            trackingOrganizationSetting,
            new List<Schema.DescribeFieldResult>{
                Tracking_Setting__c.Organization__c.getDescribe()
            }
        );

        TrackingOrganizationControllerTest.createTestOrgComponents(org.Id, trackingOrganizationSetting.Id, 2, 'In Progress', new List<String>{'Profile'});
    }

    @IsTest
    public static void retrieveTest(){
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();

        System.assertEquals(1, [SELECT Id FROM Attachment].size());

        Test.startTest();
        Test.setMock(WebServiceMock.class, new TrackingOrganizationControllerTest.WebServiceMockImpl(new Map<String, Object> {
                'isLimitError' => false
        }));
        TrackingOrganizationRetrieveBatch batch = new TrackingOrganizationRetrieveBatch(testDataContainer.trackingSettingId, testDataContainer.logger);
        Database.executeBatch(batch, 1);
        Test.stopTest();

        TrackingOrganizationControllerTest.TestResultDataContainer testResultDataContainer =
                new TrackingOrganizationControllerTest.TestResultDataContainer(testDataContainer.trackingSettingId);

        System.assertEquals(testResultDataContainer.sizeOrgComponentsAll + 1, [SELECT Id FROM Attachment].size());
        System.assertEquals(false, testResultDataContainer.isProcessed);
    }

    @IsTest
    public static void retrieveLimitTest(){
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();

        System.assertEquals(1, [SELECT Id FROM Attachment].size());
        Test.startTest();
        Test.setMock(WebServiceMock.class, new TrackingOrganizationControllerTest.WebServiceMockImpl(new Map<String, Object> {
                'isLimitError' => true
        }));
        TrackingOrganizationRetrieveBatch batch = new TrackingOrganizationRetrieveBatch(testDataContainer.trackingSettingId, testDataContainer.logger);
        Database.executeBatch(batch, 1);
        Test.stopTest();

        TrackingOrganizationControllerTest.TestResultDataContainer testResultDataContainer =
                new TrackingOrganizationControllerTest.TestResultDataContainer(testDataContainer.trackingSettingId);

        System.assertEquals(1, [SELECT Id FROM Attachment].size());
        System.assertEquals(true, testResultDataContainer.isProcessed);
    }

    @IsTest
    public static void retrieveLimitOneTest(){
        delete [SELECT Id FROM Org_Component__c LIMIT 1];
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();

        System.assertEquals(1, [SELECT Id FROM Attachment].size());
        Test.startTest();
        Test.setMock(WebServiceMock.class, new TrackingOrganizationControllerTest.WebServiceMockImpl(new Map<String, Object> {
                'isLimitError' => true
        }));
        TrackingOrganizationRetrieveBatch batch = new TrackingOrganizationRetrieveBatch(testDataContainer.trackingSettingId, testDataContainer.logger);
        Database.executeBatch(batch, 1);
        Test.stopTest();

        TrackingOrganizationControllerTest.TestResultDataContainer testResultDataContainer =
                new TrackingOrganizationControllerTest.TestResultDataContainer(testDataContainer.trackingSettingId);

        System.assertEquals(null, [SELECT Id, CRC32__c FROM Org_Component__c LIMIT 1].CRC32__c);
        System.assertEquals(testResultDataContainer.sizeOrgComponentsAll + 1, [SELECT Id FROM Attachment].size());
        System.assertEquals(false, testResultDataContainer.isProcessed);
    }

    @IsTest
    public static void retrieveCalloutException() {
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();

        System.assertEquals(1, [SELECT Id FROM Attachment].size());
        Test.startTest();
        Test.setMock(WebServiceMock.class, new TrackingOrganizationControllerTest.WebServiceMockImpl(new Map<String, Object> {
                'isCalloutException' => true
        }));
        TrackingOrganizationRetrieveBatch batch = new TrackingOrganizationRetrieveBatch(testDataContainer.trackingSettingId, testDataContainer.logger);
        Database.executeBatch(batch, 1);
        Test.stopTest();

        TrackingOrganizationControllerTest.TestResultDataContainer testResultDataContainer =
                new TrackingOrganizationControllerTest.TestResultDataContainer(testDataContainer.trackingSettingId);
        System.assertEquals(1, [SELECT Id FROM Attachment].size());
        System.assertEquals(2, testResultDataContainer.sizeOrgComponentsInProgress);
        System.assertEquals(true, testResultDataContainer.isProcessed);
    }

    @IsTest
    public static void retrieveCalloutExceptionForSingleComponent() {
        delete [SELECT Id FROM Org_Component__c LIMIT 1];
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();

        System.assertEquals(1, [SELECT Id FROM Attachment].size());
        Test.startTest();
        Test.setMock(WebServiceMock.class, new TrackingOrganizationControllerTest.WebServiceMockImpl(new Map<String, Object> {
                'isCalloutException' => true
        }));
        TrackingOrganizationRetrieveBatch batch = new TrackingOrganizationRetrieveBatch(testDataContainer.trackingSettingId, testDataContainer.logger);
        Database.executeBatch(batch, 1);
        Test.stopTest();

        TrackingOrganizationControllerTest.TestResultDataContainer testResultDataContainer =
                new TrackingOrganizationControllerTest.TestResultDataContainer(testDataContainer.trackingSettingId);

        System.assertEquals(1, [SELECT Id FROM Attachment].size());
        System.assertEquals(1, testResultDataContainer.sizeOrgComponentsTooLarge);
        System.assertEquals(0, testResultDataContainer.sizeOrgComponentsInProgress);
        System.assertEquals(false, testResultDataContainer.isProcessed);
    }

    @IsTest
    public static void scheduleTest(){
        delete [SELECT Id FROM Org_Component__c LIMIT 1];
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();

        Test.startTest();
        List<String> IdList = TrackingCheckRetrieveStatusScheduler.initialize(testDataContainer.trackingSettingId, 'Test', testDataContainer.logger);
        Test.stopTest();

        System.assert(!IdList.isEmpty());
    }
}