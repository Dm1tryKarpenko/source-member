public with sharing class TrackingAutomationScheduler implements Schedulable {
    private String trackingSettingId;
    private Boolean isUpdateLastStartRetrieval;

    public TrackingAutomationScheduler(String trackingSettingId) {
        this(trackingSettingId, true);
    }

    public TrackingAutomationScheduler(String trackingSettingId, Boolean isUpdateLastStartRetrieval) {
        this.trackingSettingId = trackingSettingId;
        this.isUpdateLastStartRetrieval = isUpdateLastStartRetrieval;
    }

    public void execute(SchedulableContext context) {
        try {

            TrackingService.Logger logger = getLogger();

            DatabaseUtils.checkObjectsAndFieldsAccessibility(new Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>>{
                    Tracking_Setting__c.getSObjectType().getDescribe() => new List<Schema.DescribeFieldResult>{
                            Tracking_Setting__c.Last_Start_Retrieval__c.getDescribe(),
                            Tracking_Setting__c.Is_Processed__c.getDescribe()
                    }
            });
            List<Tracking_Setting__c> trackingSettingList = [
                    SELECT Last_Start_Retrieval__c, Is_Processed__c
                    FROM Tracking_Setting__c
                    WHERE Id = : trackingSettingId
            ];

            if (trackingSettingList.isEmpty()) {
                throw new FlosumException('TrackingSetting not found.');
            }

            Tracking_Setting__c trackingSetting = trackingSettingList.get(0);

            if (logger != null && !trackingSetting.Is_Processed__c) {
                trackingSetting.Is_Processed__c = true;
                if (isUpdateLastStartRetrieval) {
                    trackingSetting.Last_Start_Retrieval__c = Datetime.now();
                }
                DatabaseUtils.upsertRecord(
                        trackingSetting,
                        new List<Schema.DescribeFieldResult> {
                                Tracking_Setting__c.Is_Processed__c.getDescribe(),
                                Tracking_Setting__c.Last_Start_Retrieval__c.getDescribe()
                        }
                );
                Database.executeBatch(new TrackingRequestRetrieveBatch(trackingSettingId, logger), 1);
            }
        } catch(Exception e) {
            System.debug('Was an error TrackingAutomationScheduler: ' + e.getMessage());
        }
    }

    private TrackingService.Logger getLogger() {
        TrackingService.Logger logger = null;

        DatabaseUtils.checkObjectsAndFieldsAccessibility(new Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>>{
                Tracking_Setting__c.getSObjectType().getDescribe() => new List<Schema.DescribeFieldResult>{
                        Tracking_Setting__c.Organization__c.getDescribe()
                }
        });

        List<Tracking_Setting__c> trackingOrganizationSettingList = [
                SELECT Id, Organization__c
                FROM Tracking_Setting__c
                WHERE Id =: trackingSettingId
        ];

        if (trackingOrganizationSettingList.isEmpty()) {
            throw new FlosumException('TrackingSetting not found.');
        }

        Metadata_Log__c metadataLog = TrackingService.createLog(
                trackingOrganizationSettingList.get(0).Organization__c,
                trackingOrganizationSettingList.get(0).Id,
                TrackingService.PROCESS_TYPE_RETRIEVE_ORGANIZATION, TrackingService.TRACKING_ORGANIZATION_LOG_NAME
        );

        logger = new TrackingService.Logger(metadataLog.Id, TrackingService.TRACKING_ORGANIZATION_LOG_NAME);

        return logger;
    }

}