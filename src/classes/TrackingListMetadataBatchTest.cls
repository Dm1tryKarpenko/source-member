/**
 * Created by vladislav on 15.09.21.
 */
@IsTest
public with sharing class TrackingListMetadataBatchTest {

    @TestSetup
    public static void setup() {
        Organisation__c org = new Organisation__c(
                Username__c = 'test@test.com',
                User_Full_Name__c = 'test person',
                OwnerId = UserInfo.getUserId()
        );
        insert org;

        Tracking_Setting__c trackingOrganizationSetting = new Tracking_Setting__c();
        trackingOrganizationSetting.Organization__c = org.Id;
        trackingOrganizationSetting.Is_Processed__c = true;
        trackingOrganizationSetting.Is_Enabled__c = true;
        DatabaseUtils.insertRecord(
                trackingOrganizationSetting,
                new List<Schema.DescribeFieldResult>{
                        Tracking_Setting__c.Organization__c.getDescribe(),
                        Tracking_Setting__c.Is_Processed__c.getDescribe(),
                        Tracking_Setting__c.Is_Enabled__c.getDescribe()
                }
        );
    }

    @IsTest
    public static void listMetadataTest() {
        Test.setMock(WebServiceMock.class, new TrackingOrganizationControllerTest.WebServiceMockImpl());
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();

        TrackingOrganizationControllerTest.createTestOrgComponents(
                testDataContainer.organisationId,
                testDataContainer.trackingSettingId,
                2
        );

        Org_Component__c component = [SELECT Label__c From Org_Component__c WHERE Name__c = 'TestOrgComponent0' LIMIT 1];
        component.Label__c = 'Old_Label';
        update component;

        Test.startTest();
        Database.executeBatch(new TrackingListMetadataBatch(testDataContainer.trackingSettingId, testDataContainer.logger), 1);
        Test.stopTest();

        Org_Component__c newComponent = [SELECT Label__c From Org_Component__c WHERE Id =: component.Id LIMIT 1];

        System.assertNotEquals('Old_Label', newComponent.Label__c);
    }

    @IsTest
    public static void listMetadataTestFolder() {
        Test.setMock(WebServiceMock.class, new TrackingOrganizationControllerTest.WebServiceMockImpl(new Map<String, Object> {
                'isFolder' => true
        }));
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();

        TrackingOrganizationControllerTest.createTestOrgComponents(
                testDataContainer.organisationId,
                testDataContainer.trackingSettingId,
                2,
                'Pending',
                new List<String> { 'Document' }
        );

        Org_Component__c component = [SELECT Label__c From Org_Component__c WHERE Name__c = 'TestOrgComponent0' LIMIT 1];
        component.Label__c = 'Old_Label';
        update component;

        Test.startTest();
        Database.executeBatch(new TrackingListMetadataBatch(testDataContainer.trackingSettingId, testDataContainer.logger), 1);
        Test.stopTest();

        Org_Component__c newComponent = [SELECT Label__c From Org_Component__c WHERE Id =: component.Id LIMIT 1];

        System.assertNotEquals('Old_Label', newComponent.Label__c);
    }

    @IsTest
    public static void generateManualMetadata() {
        Test.setMock(WebServiceMock.class, new TrackingOrganizationControllerTest.WebServiceMockImpl());
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();

        TrackingOrganizationControllerTest.createTestOrgComponents(
                testDataContainer.organisationId,
                testDataContainer.trackingSettingId,
                1,
                'Pending',
                new List<String> { 'ApexClass' }
        );

        Test.startTest();
        Database.executeBatch(new TrackingListMetadataBatch(testDataContainer.trackingSettingId, testDataContainer.logger), 1);
        Test.stopTest();

        Org_Component__c newComponent = [SELECT Label__c From Org_Component__c WHERE Name__c = 'TestOrgComponent0' LIMIT 1];
        TrackingOrganizationControllerTest.TestResultDataContainer testResultDataContainer =
                new TrackingOrganizationControllerTest.TestResultDataContainer(testDataContainer.trackingSettingId);
        System.assertEquals('classes/TestOrgComponent0.cls', newComponent.Label__c);
    }

    @IsTest
    public static void generateManualMetadataCustomField() {
        Test.setMock(WebServiceMock.class, new TrackingOrganizationControllerTest.WebServiceMockImpl());
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();

        TrackingOrganizationControllerTest.createTestOrgComponents(
                testDataContainer.organisationId,
                testDataContainer.trackingSettingId,
                1,
                'Pending',
                new List<String> { 'CustomField' }
        );

        Test.startTest();
        Database.executeBatch(new TrackingListMetadataBatch(testDataContainer.trackingSettingId, testDataContainer.logger), 1);
        Test.stopTest();

        Org_Component__c newComponent = [SELECT Label__c From Org_Component__c WHERE Name__c = 'TestOrgComponent0' LIMIT 1];
        TrackingOrganizationControllerTest.TestResultDataContainer testResultDataContainer =
                new TrackingOrganizationControllerTest.TestResultDataContainer(testDataContainer.trackingSettingId);
        System.assertEquals('objects/TestOrgComponent0.object', newComponent.Label__c);
    }

}