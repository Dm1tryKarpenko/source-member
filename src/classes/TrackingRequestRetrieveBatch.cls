public with sharing class TrackingRequestRetrieveBatch implements Database.Batchable<Integer>, Database.AllowsCallouts, Database.Stateful{

    private static final Integer CHUNK_SIZE = 75;

    private String accessToken;
    private String instanceUrl;
    private String trackingSettingId;
    private TrackingService.Logger logger;
    private List<String> componentIdSourceMemberList;

    private Boolean isProcessingTooLargeComponents;
    private Mode modeInstance;

    private String lastIdSourceMemberComponent;
    private Integer componentsInProcess;
    private Tracking_Setting__c trackingSetting;
    private List<String> asyncJobIdList;


    public TrackingRequestRetrieveBatch(
            String trackingSettingId,
            TrackingService.Logger logger
    ) {
        this(
                trackingSettingId,
                logger,
                new List<String>(),
                false
        );
    }

    public TrackingRequestRetrieveBatch(
            String trackingSettingId,
            TrackingService.Logger logger,
            List<String> componentIdSourceMemberList
    ) {
        this(
                trackingSettingId,
                logger,
                componentIdSourceMemberList,
                false
        );
    }

    public TrackingRequestRetrieveBatch(
            String trackingSettingId,
            TrackingService.Logger logger,
            List<String> componentIdSourceMemberList,
            Boolean isProcessingTooLargeComponents
    ) {
        if (logger == null) {
            throw new FlosumException('logger couldn\'t be null');
        }
        this.trackingSettingId = trackingSettingId;
        this.componentIdSourceMemberList = componentIdSourceMemberList;
        this.logger = logger;

        this.isProcessingTooLargeComponents = isProcessingTooLargeComponents;
        this.modeInstance = Mode.LAST_ID;
        this.lastIdSourceMemberComponent = null;
        this.componentsInProcess = 0;
        this.trackingSetting = null;
        this.asyncJobIdList = new List<String>();
    }

    public Iterable<Integer> start(Database.BatchableContext batchableContext) {
        try {
            TrackingService.checkIsTrackingIsProcess(trackingSettingId, batchableContext);
            refreshTrackingSetting();

            if (trackingSetting != null) {
                if (!trackingSetting.Is_Processed__c) return new List<Integer>();
                AuthUtils.AuthenticateDetails authDetails = AuthUtils.authenticateAnOrg(trackingSetting.Organization__c, FlosumConstants.DEVELOPER);
                accessToken = authDetails.details.AccessToken__c;
                instanceUrl = authDetails.details.InstanceURL__c;

                List<Integer> chunkSizeList = new List<Integer>();

                Decimal sourceMemberSize;
                if (componentIdSourceMemberList != null && !componentIdSourceMemberList.isEmpty()) {
                    sourceMemberSize = componentIdSourceMemberList.size();
                    modeInstance = Mode.LIST_COMPONENT_ID;
                } else {
                    sourceMemberSize = retrieveApexRestResponseWrapper();
                }

                Integer resultSize = (Integer) (sourceMemberSize / CHUNK_SIZE).round(System.RoundingMode.UP);

                for (Integer i = 1; i <= resultSize; i++) {
                    chunkSizeList.add(i);
                }

                String processRetrieve = !isProcessingTooLargeComponents
                        ? TrackingService.PROCESS_TYPE_RETRIEVE_ORGANIZATION
                        : TrackingService.PROCESS_TYPE_RETRIEVE_MANUALLY_ORGANIZATION;
                logger.addLine(processRetrieve + ' started.');
                logger.addLine('Components to process:');
                logger.updateLog();

                setApexJobIdSetting(trackingSetting.Id, batchableContext.getJobId());
                return chunkSizeList;

            } else {
                TrackingService.handleErrorLog(logger, 'Tracking Setting by id not found.');
                return new List<Integer>();
            }
        } catch (Exception e) {
            logger.addLine('Error Start Retrieve. ' + e.getMessage());
            logger.updateLog();
        }

        return new List<Integer>();
    }

    public void execute(Database.BatchableContext param1, List<Integer> componentIdsChunk) {
        TrackingService.checkIsTrackingIsProcess(trackingSettingId, param1);
        refreshTrackingSetting();

        if (trackingSetting == null || !trackingSetting.Is_Processed__c) return;

        try {
            AuthUtils.AuthenticateDetails authDetails = AuthUtils.authenticateAnOrg(trackingSetting.Organization__c, FlosumConstants.DEVELOPER);
            accessToken = authDetails.details.AccessToken__c;
            instanceUrl = authDetails.details.InstanceURL__c;

            TrackingService.SourceMemberPacksWrapper packsWrapper = retrievePacksWrapper(componentIdsChunk.get(0));

          //set lastIdSourceMemberComponent
            if (modeInstance == Mode.LAST_ID) {
                lastIdSourceMemberComponent = packsWrapper.lastId;
            }

            List<TrackingService.RetrieveComponentWrapper> retrieveComponentWrapperChunk = new List<TrackingService.RetrieveComponentWrapper>();
            if (!isProcessingTooLargeComponents) {
                Map<String, List<TrackingService.SourceMemberWrapper>> packsMap = packsWrapper.getChangedMap();
                retrieveComponentWrapperChunk = makeRetrieveComponentWrapperList(packsMap);
            } else {
                for(TrackingService.SourceMemberWrapper sourceMemberWrapper: packsWrapper.tooLargeList) {
                    TrackingService.RetrieveComponentWrapper retrieveComponentWrapper = new TrackingService.RetrieveComponentWrapper(
                            sourceMemberWrapper.componentType,
                            new List<TrackingService.SourceMemberWrapper> { sourceMemberWrapper }
                    );
                    retrieveComponentWrapperChunk.add(retrieveComponentWrapper);
                }
            }

            asyncJobIdList.addAll(TrackingService.makeRequestRetrieve(accessToken, instanceUrl, retrieveComponentWrapperChunk));

            List<TrackingService.SourceMemberWrapper> sourceMemberWrapperList = getSourceMemberWrapperList(retrieveComponentWrapperChunk);
            TrackingService.upsertOrgComponentList(sourceMemberWrapperList, trackingSetting.Id, trackingSetting.Organization__c );
            componentsInProcess += sourceMemberWrapperList.size();
            logger.updateLog();
        } catch (Exception e) {
            logger.addLine('Error Start Retrieve. ' + e.getMessage());
            logger.updateLog();
        }

    }

    public void finish(Database.BatchableContext param1) {
        try {
            TrackingService.checkIsTrackingIsProcess(trackingSettingId, param1);
            refreshTrackingSetting();
            if (!trackingSetting.Is_Processed__c) {
                setApexJobIdSetting(trackingSetting.Id, '');
                return;
            }
            if (!asyncJobIdList.isEmpty()) {
                if (String.isBlank(trackingSetting.Current_Jobs__c)) {
                    trackingSetting.Current_Jobs__c = String.join(asyncJobIdList, ',');
                } else {
                    trackingSetting.Current_Jobs__c += String.join(asyncJobIdList, ',');
                }

                DatabaseUtils.upsertRecord(
                        trackingSetting,
                        new List<Schema.DescribeFieldResult> {
                                Tracking_Setting__c.Current_Jobs__c.getDescribe(),
                                Tracking_Setting__c.Is_Processed__c.getDescribe()
                        }
                );
            }

            if (String.isNotBlank(trackingSetting.Current_Jobs__c)) {
                logger.addLine('Size components in process: ' + componentsInProcess);
                logger.addLine('Retrieve Started.');
                logger.updateLog();
                if (!Test.isRunningTest()) {
                    if (!isProcessingTooLargeComponents) {
                        Database.executeBatch(new TrackingListMetadataBatch(trackingSettingId, logger), 1);
                    }
                }
                setApexJobIdSetting(trackingSetting.Id, '');
            }  else {
                logger.updateLog();
                TrackingService.handleSuccessSettings(trackingSettingId);
                TrackingService.handleNotProcessedOrgComponents(trackingSettingId, logger);
                TrackingService.handleSuccessLog(logger, 'Retrieve completed.');
                if (!Test.isRunningTest()) TrackingService.nextRetrieve(trackingSettingId, false);
                setApexJobIdSetting(trackingSetting.Id, '');
            }
        } catch (Exception e) {
            TrackingService.handleSuccessSettings(trackingSettingId);
            TrackingService.handleNotProcessedOrgComponents(trackingSettingId, logger);
            TrackingService.handleError(trackingSettingId, logger, 'Error Start Retrieve. ' + e.getMessage());
            if (!Test.isRunningTest()) TrackingService.nextRetrieve(trackingSettingId, false);
            setApexJobIdSetting(trackingSettingId, '');
        }
    }

    private Decimal retrieveApexRestResponseWrapper() {
        Decimal sourceMemberSize = 0;

        try {
            //retrieve only size
            String endpoint = getEndpoint('', null, TrackingService.QUERY_COUNT_SOURCE_MEMBER);
            TrackingService.ApexRestResponseWrapper responseWrapper = TrackingService.callSourceMemberResponse(accessToken, endpoint);
            sourceMemberSize = responseWrapper.records.get(0).SourceMemberSize;

        } catch (Exception e) {
            logger.addLine('ERROR retrieveApexRestResponseWrapper ' + e.getMessage());
            logger.updateLog();
        }

        return sourceMemberSize;
    }

    private TrackingService.SourceMemberPacksWrapper retrievePacksWrapper(Integer chunkNumber) {
        TrackingService.SourceMemberPacksWrapper packsWrapper = new TrackingService.SourceMemberPacksWrapper();

        String nextRecordsUrl = '';

        do {
            List<String> chunkComponentIdSourceMemberList = getChunkComponentIdSourceMemberList(chunkNumber);
            String endpoint = getEndpoint(nextRecordsUrl, chunkComponentIdSourceMemberList, TrackingService.QUERY_SOURCE_MEMBER);
            TrackingService.SourceMemberPacksWrapper packsWrapperChunk = TrackingService.callSourceMemberRecords(trackingSetting.Id, accessToken, endpoint);
            packsWrapper.changedList.addAll(packsWrapperChunk.changedList);
            packsWrapper.tooLargeList.addAll(packsWrapperChunk.tooLargeList);
            packsWrapper.lastId = packsWrapperChunk.lastId;

            nextRecordsUrl = packsWrapperChunk.nextRecordUrl;

        } while (String.isNotBlank(nextRecordsUrl));


        return packsWrapper;
    }

    private String getEndpoint(String nextRecordsUrl, List<String> chunkComponentIdSourceMemberList, String query) {
        String endpoint = instanceUrl;

        if (String.isNotBlank(nextRecordsUrl)) {
            endpoint += nextRecordsUrl;
        } else {
            endpoint += '/services/data/v51.0/tooling/query/?q=' + query;
            String whereClause = modeInstance == Mode.LAST_ID
                  ? TrackingService.getWhereClause(lastIdSourceMemberComponent, trackingSetting)
                  : TrackingService.getWhereClause(chunkComponentIdSourceMemberList, trackingSetting);

            if (String.isNotBlank(whereClause)) {
                endpoint += '+AND' + whereClause;
            }
            if (query.equals(TrackingService.QUERY_SOURCE_MEMBER)) {
                endpoint += '+ORDER+BY+Id';
                endpoint += '+LIMIT+' + CHUNK_SIZE;
            }
        }

        return endpoint;
    }



    private List<TrackingService.SourceMemberWrapper> getSourceMemberWrapperList(List<TrackingService.RetrieveComponentWrapper> retrieveComponentWrapperList) {
        List<TrackingService.SourceMemberWrapper> sourceMemberWrapperList = new List<TrackingService.SourceMemberWrapper>();

        for (TrackingService.RetrieveComponentWrapper retrieveComponentWrapper : retrieveComponentWrapperList) {
            sourceMemberWrapperList.addAll(retrieveComponentWrapper.componentList);

            for (TrackingService.SourceMemberWrapper sourceMemberWrapper : retrieveComponentWrapper.componentList) {
                logger.addLine('Type: ' + retrieveComponentWrapper.type + ', Name: ' + sourceMemberWrapper.componentName);
            }
        }

        return sourceMemberWrapperList;
    }

    private List<String> getChunkComponentIdSourceMemberList(Integer chunkNumber) {
        if (componentIdSourceMemberList == null || componentIdSourceMemberList.isEmpty() || modeInstance == Mode.LAST_ID) {
            return new List<String>();
        }

        List<String> chunkComponentIdSourceMemberList = new List<String>();

        try {
            Integer endIterate = chunkNumber * CHUNK_SIZE;
            Integer startIterate = endIterate - CHUNK_SIZE;

            if (endIterate > componentIdSourceMemberList.size()) {
                endIterate = componentIdSourceMemberList.size();
            }

            for (Integer i = startIterate; i < endIterate; i++) {
                chunkComponentIdSourceMemberList.add(componentIdSourceMemberList.get(i));
            }
        } catch(Exception e) {
            logger.addLine('ERROR for get chunk id SourceMember list');
            return new List<String>();
        }

        return chunkComponentIdSourceMemberList;
    }

    private List<TrackingService.RetrieveComponentWrapper> makeRetrieveComponentWrapperList(Map<String, List<TrackingService.SourceMemberWrapper>> packsMap) {
        List<TrackingService.RetrieveComponentWrapper> retrieveComponentWrapperList = new List<TrackingService.RetrieveComponentWrapper>();

        for(String key : packsMap.keySet()) {
            List<TrackingService.SourceMemberWrapper> value = packsMap.get(key);

            TrackingService.RetrieveComponentWrapper retrieveComponentWrapper = new TrackingService.RetrieveComponentWrapper(key,value);
            if (TrackingService.TYPES_LARGE_WEIGHS_SET.contains(key)) {
                //forming List from one type for limit heap size
                List<TrackingService.RetrieveComponentWrapper> retrieveComponentWrapperOneTypeList = new List<TrackingService.RetrieveComponentWrapper>();

                ParallelizationAbstract parallelizationInstance = createParallelizationInstance(key);

                retrieveComponentWrapperOneTypeList = parallelizationInstance.parallelize(retrieveComponentWrapper);

                retrieveComponentWrapperList.addAll(retrieveComponentWrapperOneTypeList);
            } else {
                retrieveComponentWrapperList.add(retrieveComponentWrapper);
            }
        }

        return retrieveComponentWrapperList;
    }

    private void setApexJobIdSetting(String trackingSettingId, String apexJobId) {
        DatabaseUtils.checkObjectsAndFieldsAccessibility(new Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>>{
                Tracking_Setting__c.getSObjectType().getDescribe() => new List<Schema.DescribeFieldResult>{
                        Tracking_Setting__c.Id.getDescribe()
                }
        });

        List<Tracking_Setting__c> trackingOrganizationSettingList = [SELECT Id FROM Tracking_Setting__c WHERE Id =: trackingSettingId];
        if (!trackingOrganizationSettingList.isEmpty()) {
            trackingOrganizationSettingList.get(0).Current_Apex_Job_Id__c = apexJobId;
            DatabaseUtils.upsertRecord(
                    trackingOrganizationSettingList.get(0),
                    new List<Schema.DescribeFieldResult> {
                            Tracking_Setting__c.Current_Apex_Job_Id__c.getDescribe()
                    }
            );
        }
    }

    private void refreshTrackingSetting() {
        DatabaseUtils.checkObjectsAndFieldsAccessibility(new Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>>{
                Tracking_Setting__c.getSObjectType().getDescribe() => new List<Schema.DescribeFieldResult>{
                        Tracking_Setting__c.Name.getDescribe(),
                        Tracking_Setting__c.Organization__c.getDescribe(),
                        Tracking_Setting__c.Is_Processed__c.getDescribe(),
                        Tracking_Setting__c.Current_Jobs__c.getDescribe(),
                        Tracking_Setting__c.Last_Start_Retrieval__c.getDescribe(),
                        Tracking_Setting__c.Metadata_Types__c.getDescribe()
                }
        });

        List<Tracking_Setting__c> trackingOrganizationSettingList = [
                SELECT
                        Id,
                        Name,
                        Organization__c,
                        Is_Processed__c,
                        Current_Jobs__c,
                        Last_Start_Retrieval__c,
                        Metadata_Types__c,
                        Organization__r.Name
                FROM Tracking_Setting__c
                WHERE Id = : trackingSettingId
        ];
        trackingSetting = !trackingOrganizationSettingList.isEmpty()
                ? trackingOrganizationSettingList.get(0)
                : null;
    }

    private ParallelizationAbstract createParallelizationInstance(String type) {
        switch on type {
            when 'StaticResource' {
                return new ParallelizationStaticResource(instanceUrl, accessToken, logger);
            } when else {
                return new ParallelizationStandard();
            }
        }
    }

    private enum Mode { LAST_ID, LIST_COMPONENT_ID }

    public abstract class ParallelizationAbstract {
        private String instanceUrl;
        private String accessToken;
        private TrackingService.Logger logger;

        public ParallelizationAbstract() {}

        public ParallelizationAbstract(String instanceUrl, String accessToken, TrackingService.Logger logger) {
            this.instanceUrl = instanceUrl;
            this.accessToken = accessToken;
            this.logger = logger;
        }

        public abstract List<TrackingService.RetrieveComponentWrapper> parallelize(TrackingService.RetrieveComponentWrapper retrieveComponentWrapper);
    }

    public class ParallelizationStandard  extends ParallelizationAbstract {
        public override List<TrackingService.RetrieveComponentWrapper> parallelize(TrackingService.RetrieveComponentWrapper retrieveComponentWrapper) {
            List<TrackingService.RetrieveComponentWrapper> retrieveComponentWrapperList = new List<TrackingService.RetrieveComponentWrapper>();

            for (TrackingService.SourceMemberWrapper sourceMemberWrapper : retrieveComponentWrapper.componentList) {
                List<TrackingService.SourceMemberWrapper> sourceMemberWrapperList = new List<TrackingService.SourceMemberWrapper>{ sourceMemberWrapper };

                retrieveComponentWrapperList.add(new TrackingService.RetrieveComponentWrapper(retrieveComponentWrapper.type, sourceMemberWrapperList));
            }

            return retrieveComponentWrapperList;
        }
    }

    public class ParallelizationStaticResource  extends ParallelizationAbstract {
        public ParallelizationStaticResource(String instanceUrl, String accessToken, TrackingService.Logger logger) {
            super(instanceUrl, accessToken, logger);
        }

        public override List<TrackingService.RetrieveComponentWrapper> parallelize(TrackingService.RetrieveComponentWrapper retrieveComponentWrapper) {
            List<TrackingService.RetrieveComponentWrapper> retrieveComponentWrapperList = new List<TrackingService.RetrieveComponentWrapper>();
            String type = 'StaticResource';

            List<String> retrieveComponentWrapperIdList = new List<String>();
            for (TrackingService.SourceMemberWrapper sourceMemberWrapper : retrieveComponentWrapper.componentList) {
                retrieveComponentWrapperIdList.add(sourceMemberWrapper.id);
            }

            Map<Id, Integer> staticResourceIdBodyLengthMap = retrieveStaticResourceIdBodyLengthMap(retrieveComponentWrapperIdList);
            ///////////
            TrackingService.RetrieveComponentWrapper retrieveComponentWrapperChunk = new TrackingService.RetrieveComponentWrapper(type);
            Integer sizeChunk = 0;

            for (TrackingService.SourceMemberWrapper sourceMemberWrapper : retrieveComponentWrapper.componentList) {
                Integer currentSizeStaticResource = staticResourceIdBodyLengthMap.get(sourceMemberWrapper.id);

                sizeChunk += currentSizeStaticResource;

                if (sizeChunk > 2000000) {
                    Boolean isChunkComponentListEmpty = retrieveComponentWrapperChunk.componentList.isEmpty();

                    if (isChunkComponentListEmpty) {
                        retrieveComponentWrapperChunk.componentList.add(sourceMemberWrapper);
                    }

                    retrieveComponentWrapperList.add(retrieveComponentWrapperChunk);

                    retrieveComponentWrapperChunk = new TrackingService.RetrieveComponentWrapper(type);
                    sizeChunk = 0;

                    if (!isChunkComponentListEmpty) {
                        retrieveComponentWrapperChunk.componentList.add(sourceMemberWrapper);
                        sizeChunk = currentSizeStaticResource;
                    }
                } else {
                    retrieveComponentWrapperChunk.componentList.add(sourceMemberWrapper);
                }
            }

            if (!retrieveComponentWrapperChunk.componentList.isEmpty()) {
                retrieveComponentWrapperList.add(retrieveComponentWrapperChunk);
            }
            ////////////////

            return retrieveComponentWrapperList;
        }

        private Map<Id, Integer> retrieveStaticResourceIdBodyLengthMap(List<String> retrieveComponentWrapperIdList) {
            Map<Id, Integer> staticResourceIdBodyLengthMap = new Map<Id, Integer>();
            try {
                String endpoint = instanceUrl;
                endpoint += '/services/data/v51.0/query/?q=' + TrackingService.QUERY_SIZE_STATIC_RESOURCE;
                endpoint += '+WHERE+Id+IN+(\'' + String.join(retrieveComponentWrapperIdList, '\',+\'') + '\')';
                TrackingService.ApexRestResponseWrapper responseWrapper = TrackingService.callSourceMemberResponse(accessToken, endpoint);
                for (TrackingService.ApexRestResponseRecordWrapper apexRestResponseRecordWrapper: responseWrapper.records) {
                    staticResourceIdBodyLengthMap.put(apexRestResponseRecordWrapper.Id, apexRestResponseRecordWrapper.BodyLength);
                }
            } catch (Exception e) {
                logger.addLine('ERROR retrieveStaticResourceIdBodyLengthMap ' + e.getMessage());
                logger.updateLog();
            }

            return staticResourceIdBodyLengthMap;
        }
    }
}