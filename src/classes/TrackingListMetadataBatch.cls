public with sharing class TrackingListMetadataBatch implements Database.Batchable<String>, Database.AllowsCallouts {

    public static final Map<String, String> metadataFolderMap = new Map<String, String> {
        'DocumentFolder' => 'Document', 'DashboardFolder' => 'Dashboard', 'ReportFolder' => 'Report', 'EmailFolder' => 'EmailTemplate'
    };

    public static final Set<String> typesForManualGenerationMetadataSet = new Set<String> {
            'ApexClass',
            'ApexPage',
            'ApexTrigger',
            'AuraDefinitionBundle',
            'CustomField',
            'CustomLabel',
            'CustomObject',
            'CustomPermission',
            'Layout',
            'Profile',
            'RecordType',
            'RemoteSiteSetting',
            'Report',
            'StaticResource',
            'WebLink'
    };
    public static final Set<String> typesIncludedInCustomObjectSet = new Set<String> {
            'CustomField', 'RecordType', 'WebLink'
    };


    private String trackingSettingId;
    private TrackingService.Logger logger;

    public TrackingListMetadataBatch(String trackingSettingId, TrackingService.Logger logger) {
        if (logger == null) {
            throw new FlosumException('logger couldn\'t be null');
        }
        this.trackingSettingId = trackingSettingId;
        this.logger = logger;
    }


    public Iterable<String> start(Database.BatchableContext param1) {
        try {
            TrackingService.checkIsTrackingIsProcess(trackingSettingId, param1);

            DatabaseUtils.checkObjectsAndFieldsAccessibility(new Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>>{
                Tracking_Setting__c.getSObjectType().getDescribe() => new List<Schema.DescribeFieldResult>{
                    Tracking_Setting__c.Organization__c.getDescribe(),
                    Tracking_Setting__c.Current_Jobs__c.getDescribe()
                }
            });

            List<Tracking_Setting__c> trackingSettingList = [
                SELECT Organization__c, Organization__r.Name, Current_Jobs__c
                FROM Tracking_Setting__c
                WHERE Id = : trackingSettingId
            ];

            if (!trackingSettingList.isEmpty()) {
                Set<String> typeSet = new Set<String>();

                DatabaseUtils.checkObjectsAndFieldsAccessibility(new Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>>{
                    Org_Component__c.getSObjectType().getDescribe() => new List<Schema.DescribeFieldResult>{
                        Org_Component__c.Component_Type__c.getDescribe()
                    }
                });

                for (Org_Component__c orgComponent : [
                    SELECT Component_Type__c
                    FROM Org_Component__c
                    WHERE Is_Processed__c = TRUE
                    AND Tracking_Setting__c = :trackingSettingId
                    AND Status__c = 'In Progress'
                    ORDER BY Component_Type__c
                ]) {
                    typeSet.add(orgComponent.Component_Type__c);
                }

                logger.addLine('Start get metadata info.');
                logger.updateLog();
                return new List<String>(typeSet);
            } else {
                TrackingService.handleErrorLog(logger, 'Tracking Setting by id not found.');
                return new List<String>();
            }
        } catch (Exception e) {
            logger.addLine('Error TrackingListMetadataBatch start: ' + e.getMessage());
            logger.updateLog();
            return new List<String>();
        }
    }

    public void execute(Database.BatchableContext param1, List<String> typeList) {
        TrackingService.checkIsTrackingIsProcess(trackingSettingId, param1);
        String processedType = typeList.get(0);
        try {
            DatabaseUtils.checkObjectsAndFieldsAccessibility(new Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>>{
                Tracking_Setting__c.getSObjectType().getDescribe() => new List<Schema.DescribeFieldResult>{
                    Tracking_Setting__c.Organization__c.getDescribe(),
                    Tracking_Setting__c.Current_Jobs__c.getDescribe()
                }
            });

            List<Tracking_Setting__c> trackingSettingList = [
                SELECT Organization__c, Organization__r.Name, Current_Jobs__c
                FROM Tracking_Setting__c
                WHERE Id = : trackingSettingId
            ];

            if (!trackingSettingList.isEmpty()) {
                List<Org_Component__c> orgComponentList = [
                        SELECT Name__c
                        FROM Org_Component__c
                        WHERE Is_Processed__c = TRUE
                        AND Status__c = 'In Progress'
                        AND Tracking_Setting__c = :trackingSettingId
                        AND Component_Type__c = : processedType
                ];

                Boolean isNeedManualGenerationMetadata = typesForManualGenerationMetadataSet.contains(processedType);
                if (!isNeedManualGenerationMetadata) {
                    try {
                        AuthUtils.AuthenticateDetails authDetails = AuthUtils.authenticateAnOrg(trackingSettingList.get(0).Organization__c, FlosumConstants.DEVELOPER);

                        MetadataService.MetadataPort service = MetadataUtils.createService(
                                authDetails.details.AccessToken__c,
                                authDetails.details.InstanceURL__c
                        );

                        List<MetadataService.ListMetadataQuery> queries = new List<MetadataService.ListMetadataQuery>();

                        if (FlosumConstants.metadataToFolderMap.containsKey(processedType)) {
                            Set<String> folderSet = new Set<String>();
                            DatabaseUtils.checkObjectsAndFieldsAccessibility(new Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>>{
                                    Org_Component__c.getSObjectType().getDescribe() => new List<Schema.DescribeFieldResult>{
                                            Org_Component__c.Folder__c.getDescribe()
                                    }
                            });
                            for (Org_Component__c orgComponent : [
                                    SELECT Folder__c
                                    FROM Org_Component__c
                                    WHERE Is_Processed__c = TRUE
                                    AND Tracking_Setting__c = :trackingSettingId
                                    AND Status__c = 'In Progress'
                                    AND Component_Type__c = : processedType
                                    ORDER BY Component_Type__c
                            ]) {
                                folderSet.add(orgComponent.Folder__c);
                            }
                            for (String folder : folderSet) {
                                MetadataService.ListMetadataQuery queryLayout = new MetadataService.ListMetadataQuery();
                                queryLayout.folder = folder;
                                queryLayout.type_x = processedType;
                                queries.add(queryLayout);
                            }
                        } else {
                            MetadataService.ListMetadataQuery queryLayout = new MetadataService.ListMetadataQuery();
                            queryLayout.folder = '';
                            queryLayout.type_x = processedType;
                            queries.add(queryLayout);
                        }

                        MetadataService.FileProperties[] filePropertyList = service.listMetadata(queries, FlosumConstants.apiversion);

                        if (filePropertyList != null && !filePropertyList.isEmpty()) {
                            DatabaseUtils.checkObjectsAndFieldsAccessibility(new Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>>{
                                    Org_Component__c.getSObjectType().getDescribe() => new List<Schema.DescribeFieldResult>{
                                            Org_Component__c.Name.getDescribe()
                                    }
                            });

                            for (MetadataService.FileProperties fileProperty : filePropertyList) {
                                fileProperty = FlosumConstants.fixNamesOfItems(fileProperty);

                                System.debug('fileProperty.type_x ' + fileProperty.type_x);
                                for (Org_Component__c orgComponent : orgComponentList) {
                                    Boolean isEquals =  FlosumConstants.metadataToFolderMap.containsKey(fileProperty.type_x) && fileProperty.fullName.contains('.')
                                            ? orgComponent.Name__c == fileProperty.fullName.split('\\.').get(0)
                                            : orgComponent.Name__c == fileProperty.fullName;

                                    if (isEquals) orgComponent.Label__c = fileProperty.fileName;
                                }
                            }
                        }
                    } catch (Exception e) {
                        logger.addLine('An error occurred while trying to get metadata for Type: ' + processedType);
                        logger.addLine('The metadata will be generate manually');
                        generateManualMetadata(orgComponentList, processedType);
                    }
                } else {
                    generateManualMetadata(orgComponentList, processedType);
                }

                DatabaseUtils.upsertRecords(
                        orgComponentList,
                        new List<Schema.DescribeFieldResult>{
                                Org_Component__c.Label__c.getDescribe(),
                                Org_Component__c.Last_Updated_By__c.getDescribe(),
                                Org_Component__c.Last_Modified_Date__c.getDescribe()
                        }
                );
            }

            logger.addLine('Metadata info received for ' + processedType);
            logger.updateLog();
        } catch (Exception e) {
            logger.addLine('Error Update Metadata Info For \'' + processedType + '\': ' + e.getMessage());
            logger.updateLog();
        }
    }

    public void finish(Database.BatchableContext param1) {
        try {
            TrackingService.checkIsTrackingIsProcess(trackingSettingId, param1);
            logger.addLine('End get metadata info.');
            logger.updateLog();
            if (!Test.isRunningTest()) Database.executeBatch(new TrackingCheckRetrieveStatusBatch(trackingSettingId, logger), 50);
        } catch (Exception e) {
            TrackingService.handleSuccessSettings(trackingSettingId);
            TrackingService.handleNotProcessedOrgComponents(trackingSettingId, logger);
            TrackingService.handleError(trackingSettingId, logger, 'Error Get Metadata. ' + e.getMessage());
            if (!Test.isRunningTest()) TrackingService.nextRetrieve(trackingSettingId, false);
        }
    }

    public static List<Org_Component__c> generateManualMetadata(List<Org_Component__c> orgComponentList, String type) {
        if (String.isBlank(type)) throw new FlosumException('Type is blank');

        String folder = FlosumConstants.metaTypeFolderMap.get(type);
        String suffix = FlosumConstants.metaTypeSuffixMap.get(type);

        if (type == 'CustomLabel') {
            for (Org_Component__c orgComponent: orgComponentList) orgComponent.Label__c = 'labels/CustomLabels.labels';
        } else if (type == 'AuraDefinitionBundle') {
            for (Org_Component__c orgComponent: orgComponentList) {
                orgComponent.Label__c = String.format('{0}/{1}', new String[] { folder, orgComponent.Name__c });
            }
        } else if (typesIncludedInCustomObjectSet.contains(type)) {
            folder = FlosumConstants.metaTypeFolderMap.get('CustomObject');
            suffix = FlosumConstants.metaTypeSuffixMap.get('CustomObject');
            for (Org_Component__c orgComponent: orgComponentList) {
                orgComponent.Label__c = String.format('{0}/{1}.{2}', new String[] { folder, orgComponent.Name__c.split('\\.')[0], suffix });
            }
        } else if (String.isNotBlank(folder) && String.isNotBlank(suffix)) {
            for (Org_Component__c orgComponent: orgComponentList) {
                orgComponent.Label__c = String.format('{0}/{1}.{2}', new String[] { folder, orgComponent.Name__c, suffix });
            }
        }

        return  orgComponentList;
    }


    // If you want add new Type for Manual generate Metadata, uncomment and use this method.
    // To use this method, you must have the correct labels for the components in organisation,
    // pass organisationId and type to the method, and if method return empty map,
    // then the generation for the new type was successful
//    public static Map<String, String> checkingNewTypeForManualMetadataGeneration (String organisationId, String type) {
//        List<Org_Component__c> orgComponentWithRightLabelsList = [
//                SELECT Name__c, Label__c
//                FROM Org_Component__c
//                WHERE  OrganisationName__c =: organisationId
//                AND Component_Type__c =: type
//                AND Label__c != ''
//                LIMIT 10000
//        ];
//
//        if (orgComponentWithRightLabelsList.isEmpty()) throw new FlosumException('Org component list from org is empty');
//
//        Map<String, String> nameRightLabelMap = new Map<String, String>();
//
//        for (Org_Component__c orgComponent: orgComponentWithRightLabelsList) {
//            nameRightLabelMap.put(orgComponent.Name__c, orgComponent.Label__c);
//            orgComponent.Label__c = '';
//        }
//
//        List<Org_Component__c> orgComponentManualLabelList = TrackingListMetadataBatch.generateManualMetadata(orgComponentWithRightLabelsList, type);
//
//        Boolean isEqual = true;
//        Map<String, String> notEqualLabelsMap = new Map<String, String>();
//        for (Org_Component__c orgComponent: orgComponentManualLabelList) {
//            String rightLabel = nameRightLabelMap.get(orgComponent.Name__c);
//            if (orgComponent.Label__c != rightLabel) {
//                isEqual = false;
//                notEqualLabelsMap.put(rightLabel, orgComponent.Label__c);
//            }
//        }
//        return notEqualLabelsMap;
//    }
}