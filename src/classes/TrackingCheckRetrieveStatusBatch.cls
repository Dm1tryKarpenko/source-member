/**
 * Created by vladislav on 30.07.21.
 */

public with sharing class TrackingCheckRetrieveStatusBatch implements Database.Batchable<String>, Database.AllowsCallouts, Database.Stateful{

    private String trackingSettingId;
    private TrackingService.Logger logger;

    private Boolean isAllJobCompleted;
    private Tracking_Setting__c trackingSetting;
    private MetadataService.MetadataPort service;

    public TrackingCheckRetrieveStatusBatch(String trackingSettingId, TrackingService.Logger logger) {
        if (logger == null) {
            throw new FlosumException('logger couldn\'t be null');
        }
        this.trackingSettingId = trackingSettingId;
        this.logger = logger;

        this.isAllJobCompleted = false;
        this.trackingSetting = null;
        this.service = null;
    }

    public Iterable<String> start(Database.BatchableContext param1) {
        try {
            TrackingService.checkIsTrackingIsProcess(trackingSettingId, param1);
            DatabaseUtils.checkObjectsAndFieldsAccessibility(new Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>>{
                    Tracking_Setting__c.getSObjectType().getDescribe() => new List<Schema.DescribeFieldResult>{
                            Tracking_Setting__c.Organization__c.getDescribe(),
                            Tracking_Setting__c.Current_Jobs__c.getDescribe()
                    }
            });

            List<Tracking_Setting__c> trackingSettingList = [
                    SELECT Organization__c, Organization__r.Name, Current_Jobs__c
                    FROM Tracking_Setting__c
                    WHERE Id = : trackingSettingId
            ];

            if (!trackingSettingList.isEmpty()) {
                trackingSetting = trackingSettingList.get(0);
                if (String.isNotBlank(trackingSetting.Current_Jobs__c)) {
                    String currentJobs = trackingSettingList.get(0).Current_Jobs__c;
                    isAllJobCompleted = true;
                    AuthUtils.AuthenticateDetails authDetails = AuthUtils.authenticateAnOrg(trackingSetting.Organization__c, FlosumConstants.DEVELOPER);
                    service = MetadataUtils.createService(
                            authDetails.details.AccessToken__c,
                            authDetails.details.InstanceURL__c
                    );

                    List<String> asyncJobList = currentJobs.split(',');
                    return asyncJobList;
                }
            }
        } catch (Exception e) {
            logger.addLine('Error Check Retrieve Status. Start: ' + e.getMessage());
            logger.updateLog();
        }
        return new List<String>();
    }

    public void execute(Database.BatchableContext param1, List<String> asyncJobChunk) {
        TrackingService.checkIsTrackingIsProcess(trackingSettingId, param1);
        if (!isAllJobCompleted) return;

        try {
            String jobId = '*here should be job id*';
            for (String asyncJob : asyncJobChunk) {
                try {
                    List<String> jobData = asyncJob.split('#');
                    jobId = jobData.get(0);
                    MetadataService.RetrieveResult retrieveResult = service.checkRetrieveStatus(jobId, false);
                    if (!retrieveResult.done) {
                        isAllJobCompleted = false;
                        break;
                    }
                } catch (Exception e) {
                    logger.addLine('Error Check Retrieve Status ' + e);
                }
            }
            logger.updateLog();
        } catch(Exception e) {
            logger.addLine('Error Check Retrieve Status. Execute: ' + e.getMessage());
            logger.updateLog();
        }
    }

    public void finish(Database.BatchableContext param1) {
        try {
            TrackingService.checkIsTrackingIsProcess(trackingSettingId, param1);
            DatabaseUtils.updateRecord(
                    trackingSetting,
                    new List<Schema.DescribeFieldResult>{
                            Tracking_Setting__c.Current_Jobs__c.getDescribe()
                    }
            );

            logger.addLine('Check Retrieval Status: ' + (isAllJobCompleted
                    ? 'Completed.'
                    : 'Not Completed. Next check after 10 seconds.'));
            logger.updateLog();
            if (!Test.isRunningTest()) {
                if (isAllJobCompleted) {
                    Database.executeBatch(new TrackingOrganizationRetrieveBatch(trackingSettingId, logger), 1);
                } else {
                    TrackingCheckRetrieveStatusScheduler.initialize(trackingSettingId, trackingSetting.Organization__c, logger);
                }
            }
        } catch (Exception e) {
            TrackingService.handleSuccessSettings(trackingSettingId);
            TrackingService.handleNotProcessedOrgComponents(trackingSettingId, logger);
            TrackingService.handleError(trackingSettingId, logger, 'Error Check Retrieve Status. ' + e.getMessage());
            if (!Test.isRunningTest()) TrackingService.nextRetrieve(trackingSettingId, false);
        }
    }
}