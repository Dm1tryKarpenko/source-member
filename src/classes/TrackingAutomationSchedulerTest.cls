/**
 * Created by vladislav on 18.08.21.
 */

@IsTest
public with sharing class TrackingAutomationSchedulerTest {
    public static String CRON_EXP = '0 0 0 3 9 ? 2022';
    @TestSetup
    public static void setup() {
        Organisation__c org = new Organisation__c(
                Username__c = 'test@test.com',
                Name = 'test',
                User_Full_Name__c = 'test person',
                OwnerId = UserInfo.getUserId()
        );
        insert org;

        Tracking_Setting__c trackingOrganizationSetting = new Tracking_Setting__c();
        trackingOrganizationSetting.Organization__c = org.Id;
        trackingOrganizationSetting.Is_Processed__c = false;
        trackingOrganizationSetting.Metadata_Types__c = TrackingRequestRetrieveBatchTest.ALL_METATYPES;
        trackingOrganizationSetting.Current_Jobs__c = '0MZ0t00000JHCAgGAP#ApexClass,0MZ0t00000JHCAiGAP#Profile';

        insert trackingOrganizationSetting;
    }

    @IsTest
    public static void standardLaunch() {
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();

        Test.startTest();
        String jobId = System.schedule(
                'Tracking Automation ' + testDataContainer.organisationId,
                CRON_EXP,
                new TrackingAutomationScheduler(testDataContainer.trackingSettingId)
        );
        Test.stopTest();

        TrackingOrganizationControllerTest.TestResultDataContainer results =
                new TrackingOrganizationControllerTest.TestResultDataContainer(testDataContainer.trackingSettingId);

        System.assertEquals(true, results.isProcessed);
    }

    @IsTest
    public static void trackingSettingWrong() {
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();

        Test.startTest();
        String jobId = System.schedule(
                'Tracking Automation ' + testDataContainer.organisationId,
                CRON_EXP,
                new TrackingAutomationScheduler('1234')
        );
        Test.stopTest();

        TrackingOrganizationControllerTest.TestResultDataContainer results =
                new TrackingOrganizationControllerTest.TestResultDataContainer(testDataContainer.trackingSettingId);

        System.assertEquals(false, results.isProcessed);
    }

    @IsTest
    public static void trackingSettingEmpty() {
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();

        Test.startTest();
        String jobId = System.schedule(
                'Tracking Automation ' + testDataContainer.organisationId,
                CRON_EXP,
                new TrackingAutomationScheduler('')
        );
        Test.stopTest();

        TrackingOrganizationControllerTest.TestResultDataContainer results =
                new TrackingOrganizationControllerTest.TestResultDataContainer(testDataContainer.trackingSettingId);

        System.assertEquals(false, results.isProcessed);
    }

    @IsTest
    public static void trackingSettingNull() {
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();

        Test.startTest();
        String jobId = System.schedule(
                'Tracking Automation ' + testDataContainer.organisationId,
                CRON_EXP,
                new TrackingAutomationScheduler(null)
        );
        Test.stopTest();

        TrackingOrganizationControllerTest.TestResultDataContainer results =
                new TrackingOrganizationControllerTest.TestResultDataContainer(testDataContainer.trackingSettingId);

        System.assertEquals(false, results.isProcessed);
    }
}