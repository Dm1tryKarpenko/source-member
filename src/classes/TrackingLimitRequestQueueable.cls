public with sharing class TrackingLimitRequestQueueable implements Queueable, Database.AllowsCallouts {

    private Map<String, List<String>> limitAsyncJobComponentIdMap;
    private TrackingService.Logger logger;
    private String trackingSettingId;

    public TrackingLimitRequestQueueable(Map<String, List<String>> limitAsyncJobComponentIdMap, String trackingSettingId, TrackingService.Logger logger) {
        this.limitAsyncJobComponentIdMap = limitAsyncJobComponentIdMap;
        this.trackingSettingId = trackingSettingId;
        this.logger = logger;
    }

    public void execute(QueueableContext param1) {
        try {

            DatabaseUtils.checkObjectsAndFieldsAccessibility(new Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>>{
                    Tracking_Setting__c.getSObjectType().getDescribe() => new List<Schema.DescribeFieldResult>{
                            Tracking_Setting__c.Organization__c.getDescribe(),
                            Tracking_Setting__c.Current_Jobs__c.getDescribe()
                    }
            });

            List<Tracking_Setting__c> trackingSettingList = [
                    SELECT Organization__c, Organization__r.Name, Current_Jobs__c
                    FROM Tracking_Setting__c
                    WHERE Id = : trackingSettingId
                    AND Is_Processed__c = TRUE
            ];
            if (!trackingSettingList.isEmpty()) {
                Tracking_Setting__c trackingSetting = trackingSettingList.get(0);
                AuthUtils.AuthenticateDetails authDetails = AuthUtils.authenticateAnOrg(trackingSettingList.get(0).Organization__c, FlosumConstants.DEVELOPER);
                List<String> asyncJobList = new List<String>();
                for (String jobId : limitAsyncJobComponentIdMap.keySet()) {
                    asyncJobList.addAll(
                            TrackingService.makeLimitMetadataRequest(
                                    authDetails.details.AccessToken__c,
                                    authDetails.details.InstanceURL__c,
                                    limitAsyncJobComponentIdMap.get(jobId),
                                    logger
                            )
                    );
                }

                if (!asyncJobList.isEmpty()) {
                    trackingSetting.Current_Jobs__c = String.join(asyncJobList, ',');
                    trackingSetting.Is_Processed__c = true;


                    DatabaseUtils.upsertRecord(
                            trackingSetting,
                            new List<Schema.DescribeFieldResult> {
                                    Tracking_Setting__c.Current_Jobs__c.getDescribe(),
                                    Tracking_Setting__c.Is_Processed__c.getDescribe()
                            }
                    );

                    logger.addLine('Retrieve Started.');
                    logger.updateLog();
                    TrackingCheckRetrieveStatusScheduler.initialize(trackingSettingId, trackingSetting.Organization__c, logger);
                } else {
                    TrackingService.handleSuccessSettings(trackingSettingId);
                    TrackingService.handleNotProcessedOrgComponents(trackingSettingId, logger);
                    TrackingService.handleSuccessLog(logger, 'Retrieve completed.');
                    if (!Test.isRunningTest()) TrackingService.nextRetrieve(trackingSettingId, false);
                }
            }
            logger.updateLog();

        } catch (Exception e) {
            TrackingService.handleSuccessSettings(trackingSettingId);
            TrackingService.handleNotProcessedOrgComponents(trackingSettingId, logger);
            TrackingService.handleError(trackingSettingId, logger, 'Error TrackingLimitRequestQueueable. ' + JSON.serialize(e));
            if (!Test.isRunningTest()) TrackingService.nextRetrieve(trackingSettingId, false);
        }
    }

}