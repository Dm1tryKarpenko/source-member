@isTest
private class FlosumUtilsTest 
{
	static testMethod void myUnitTest() 
    {
    	Test.setMock(HttpCalloutMock.class, new MetadataUtils.MockHttpResponseGenerator());
        Test.setMock(WebServiceMock.class, new MetadataUtils.WebServiceMockImpl());
        
        Credentials__c cred = new Credentials__c(ConsumerSecret__c='123456987',
        										Redirect_URL__c='001',
        										Consumer_Key_Auto__c = 'key',
		   										Consumer_Secret_Auto__c='secret',
        										ConsumerKey__c='1234569874563214569513578264');
        insert cred;
        Organisation__c org = new Organisation__c(Name='Test Org',Username__c = 'test@testOrg.com');
        insert org;
        AuthorizationDetails__c auth = new AuthorizationDetails__c(AuthorisedUser__c='test@testOrg.com', Name='test@testOrg.com', AccessToken__c='test', RefreshToken__c='test', InstanceURL__c='test');
        insert auth;
        
        Test.startTest();
        FlosumUtils flosumUtil = new FlosumUtils();
        flosumUtil.authenticateAnOrg(org.Id);
        Test.stopTest();
        System.assertNotEquals(org.Id, null);
    }

    @IsTest
    public static void runMaintenanceSchedulerTest(){ 

        Credentials__c cred = new Credentials__c(ConsumerSecret__c='123456987',
        										Redirect_URL__c='001',
        										Consumer_Key_Auto__c = 'key',
		   										Consumer_Secret_Auto__c='secret',
        										ConsumerKey__c='1234569874563214569513578264');
        insert cred;
        Organisation__c org = new Organisation__c(Name='Test Org',Username__c = 'test@testOrg.com');
        insert org;
        AuthorizationDetails__c auth = new AuthorizationDetails__c(AuthorisedUser__c='test@testOrg.com', Name='test@testOrg.com', AccessToken__c='test', RefreshToken__c='test', InstanceURL__c='test');
        insert auth;

        Test.startTest();
        String jobId = FlosumUtils.runMaintenanceScheduler();
        Test.stopTest();
        System.assertNotEquals(null, org.Id);
    }
}