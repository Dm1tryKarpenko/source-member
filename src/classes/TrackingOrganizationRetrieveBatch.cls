public with sharing class TrackingOrganizationRetrieveBatch implements Database.Batchable<String>, Database.AllowsCallouts, Database.Stateful {

    public List <String> sizeErrorMessageLimit = new List<String>{
        'IO Exception: Exceeded max size limit of 3000000',
        'IO Exception: Exceeded max size limit of 6000000',
        'Web service callout failed: String length exceeds maximum:  1000000',
        'IO Exception: Exceeded max size limit of 12000000',
        'Apex heap size too large',
        'INVALID_LOCATOR: Retrieve result has been deleted.'
    };

    public List<String> tooLargeLimitList = new List<String>{
        'IO Exception: Exceeded max size limit of 12000000'
    };
    private Boolean isLimitError = false;
    private Map<String, List<String>> limitAsyncJobComponentIdMap = new Map<String, List<String>>();

    private String trackingSettingId;
    private TrackingService.Logger logger;


    public TrackingOrganizationRetrieveBatch(String trackingSettingId, TrackingService.Logger logger) {
        if (logger == null) {
            throw new FlosumException('logger couldn\'t be null');
        }
        this.trackingSettingId = trackingSettingId;
        this.logger = logger;
    }

    public Iterable<String> start(Database.BatchableContext param1) {
        try {
            TrackingService.checkIsTrackingIsProcess(trackingSettingId, param1);
            DatabaseUtils.checkObjectsAndFieldsAccessibility(new Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>>{
                Tracking_Setting__c.getSObjectType().getDescribe() => new List<Schema.DescribeFieldResult>{
                    Tracking_Setting__c.Current_Jobs__c.getDescribe()
                }
            });

            List<Tracking_Setting__c> trackingSettingList = [
                SELECT Current_Jobs__c
                FROM Tracking_Setting__c
                WHERE Id = :trackingSettingId
            ];

            if (!trackingSettingList.isEmpty()) {
                return trackingSettingList.get(0).Current_Jobs__c.split(',');
            } else {
                return new List<String>();
            }
        } catch (Exception e) {
            logger.addLine('Error Start Process Retrieved Metadata: ' + e.getMessage());
            logger.updateLog();
            return new List<String>();
        }
    }

    public void execute(Database.BatchableContext param1, List<String> jobList) {
        TrackingService.checkIsTrackingIsProcess(trackingSettingId, param1);
        List<String> jobData = jobList.get(0).split('#');

        if (jobData.size() < 2) return;

        String jobId = jobData.get(0);
        String type = jobData.get(1);

        try {
            DatabaseUtils.checkObjectsAndFieldsAccessibility(new Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>>{
                Tracking_Setting__c.getSObjectType().getDescribe() => new List<Schema.DescribeFieldResult>{
                    Tracking_Setting__c.Current_Jobs__c.getDescribe(),
                    Tracking_Setting__c.Organization__c.getDescribe()
                }
            });

            List<Tracking_Setting__c> trackingSettingList = [
                SELECT Current_Jobs__c, Organization__c
                FROM Tracking_Setting__c
                WHERE Id = :trackingSettingId
            ];

            if (!trackingSettingList.isEmpty()) {
                String organizationId = trackingSettingList.get(0).Organization__c;
                AuthUtils.AuthenticateDetails authDetails = AuthUtils.authenticateAnOrg(organizationId, FlosumConstants.DEVELOPER);

                MetadataService.MetadataPort service = MetadataUtils.createService(
                    authDetails.details.AccessToken__c,
                    authDetails.details.InstanceURL__c
                );

                Boolean isLimitCalloutTooLarge = false;
                MetadataService.RetrieveResult retrieveResult = null;
                try {
                    retrieveResult = service.checkRetrieveStatus(jobId, true);
                } catch (Exception e) {
                    for (String message : tooLargeLimitList) {
                        if (e.getMessage().contains(message)) {
                            isLimitCalloutTooLarge = true;
                            break;
                        }
                    }
                    if (!isLimitCalloutTooLarge || type != 'Profile') {
                        throw new FlosumException(e.getMessage());
                    }
                }

                if ((isLimitCalloutTooLarge || getIsLimitTooLarge(retrieveResult)) && type == 'Profile') {

                    List<Org_Component__c> orgComponentList = [
                            SELECT Id, Component_Type__c, Name__c, Label__c, Last_Updated_By__c, Last_Modified_Date__c, New_Revision_Counter__c,
                                    Attachment_ID__c
                                    FROM Org_Component__c
                            WHERE Is_Processed__c = TRUE
                            AND Component_Type__c = : type
                            AND Status__c = 'In Progress'
                            AND Tracking_Setting__c = :trackingSettingId
                            AND Async_Job_Id__c = :jobId
                    ];

                    if (orgComponentList.size() > 1) {
                        limitAsyncJobComponentIdMap.put(jobId, new List<String>());

                        for (Org_Component__c orgComponent : orgComponentList) {
                            limitAsyncJobComponentIdMap.get(jobId).add(orgComponent.Id);
                        }
                        throw new FlosumException('IO Exception: Exceeded max size limit of 12000000');
                    } else if (!orgComponentList.isEmpty()) {
                        createTooLargeComponent(orgComponentList.get(0));
                        logger.addLine('Have to be retrieve manually:');
                        logger.addLine('Type: ' + orgComponentList.get(0).Component_Type__c + ', Name: ' + orgComponentList.get(0).Name__c);
                    }
                } else if (retrieveResult != null && retrieveResult.done) {
                    if (retrieveResult.status == 'Succeeded') {

                        DatabaseUtils.checkObjectsAndFieldsAccessibility(new Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>>{
                            Org_Component__c.getSObjectType().getDescribe() => new List<Schema.DescribeFieldResult>{
                                Org_Component__c.Component_Type__c.getDescribe(),
                                Org_Component__c.Name__c.getDescribe(),
                                Org_Component__c.Label__c.getDescribe(),
                                Org_Component__c.Last_Updated_By__c.getDescribe(),
                                Org_Component__c.Last_Modified_Date__c.getDescribe(),
                                Org_Component__c.New_Revision_Counter__c.getDescribe(),
                                Org_Component__c.Attachment_ID__c.getDescribe()
                            }
                        });

                        List<Org_Component__c> orgComponentList = [
                            SELECT Id, Component_Type__c, Name__c, Label__c, Last_Updated_By__c, Last_Modified_Date__c, New_Revision_Counter__c,
                                Attachment_ID__c
                            FROM Org_Component__c
                            WHERE Is_Processed__c = TRUE
                            AND Component_Type__c = : type
                            AND Status__c = 'In Progress'
                            AND Tracking_Setting__c = :trackingSettingId
                            AND Async_Job_Id__c = :jobId
                        ];
                        if (((String) retrieveResult.zipFile).length() > 3000000 ||
                            (retrieveResult.errorMessage != null && retrieveResult.errorMessage.contains('LIMIT_EXCEEDED'))) {
                            if (orgComponentList.size() > 1) {
                                limitAsyncJobComponentIdMap.put(jobId, new List<String>());

                                for (Org_Component__c orgComponent : orgComponentList) {
                                    limitAsyncJobComponentIdMap.get(jobId).add(orgComponent.Id);
                                }
                                throw new FlosumException('IO Exception: Exceeded max size limit of 3000000');

                            } else if (!orgComponentList.isEmpty()){
                                Org_Component__c orgComponent = orgComponentList.get(0);

                                DatabaseUtils.checkObjectsAndFieldsAccessibility(new Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>>{
                                    Attachment.getSObjectType().getDescribe() => new List<Schema.DescribeFieldResult>{
                                        Attachment.Id.getDescribe()
                                    }
                                });

                                Map<Id, Attachment> attachmentMap = new Map<Id, Attachment>([
                                    SELECT Id
                                    FROM Attachment
                                    WHERE Id = : orgComponent.Attachment_ID__c
                                ]);

                                Attachment largeAttachment;
                                if (orgComponent.Attachment_ID__c != null && attachmentMap.containsKey(orgComponent.Attachment_ID__c)) {
                                    largeAttachment = attachmentMap.get(orgComponent.Attachment_ID__c);
                                } else {
                                    largeAttachment = new Attachment();
                                    largeAttachment.ParentId = organizationId;
                                }

                                largeAttachment.Name = orgComponent.Name__c;
                                largeAttachment.ContentType = 'application/zip';
                                largeAttachment.Body = EncodingUtil.base64Decode(retrieveResult.zipFile);
                                largeAttachment.Description = orgComponent.Component_Type__c;

                                DatabaseUtils.upsertRecord(
                                    largeAttachment,
                                    new List<Schema.DescribeFieldResult>{
                                        Attachment.Body.getDescribe(),
                                        Attachment.Name.getDescribe(),
                                        Attachment.Description.getDescribe(),
                                        Attachment.ContentType.getDescribe()
                                    }
                                );

                                orgComponent.Is_Processed__c = false;
                                orgComponent.Is_Retrieved__c = true;
                                orgComponent.Status__c = 'Retrieved';
                                orgComponent.Attachment_ID__c = largeAttachment.Id;
                                orgComponent.Async_Job_Id__c = null;

                                if (orgComponent.New_Revision_Counter__c != null) {
                                    orgComponent.Revision_Counter__c = orgComponent.New_Revision_Counter__c;
                                    orgComponent.New_Revision_Counter__c = null;
                                }

                                DatabaseUtils.upsertRecords(
                                    orgComponentList,
                                    new List<Schema.DescribeFieldResult>{
                                        Org_Component__c.Name__c.getDescribe(),
                                        Org_Component__c.API_Name__c.getDescribe(),
                                        Org_Component__c.Label__c.getDescribe(),
                                        Org_Component__c.Component_Type__c.getDescribe(),
                                        Org_Component__c.Last_Updated_By__c.getDescribe(),
                                        Org_Component__c.Last_Modified_Date__c.getDescribe(),
                                        Org_Component__c.Attachment_ID__c.getDescribe()
                                    }
                                );
                            } else {
                                logger.addLine(type + ', records to processed not found');
                            }

                        } else {
                            Zippex zippex = new Zippex(EncodingUtil.base64Decode((String) retrieveResult.zipFile));

                            DatabaseUtils.checkObjectsAndFieldsAccessibility(new Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>>{
                                Org_Component__c.getSObjectType().getDescribe() => new List<Schema.DescribeFieldResult>{
                                    Org_Component__c.Attachment_ID__c.getDescribe()
                                }
                            });

                            Set<Id> attachmentIdSet = new Set<Id>();

                            for (Org_Component__c orgComponent : orgComponentList) {
                                if (orgComponent.Attachment_ID__c != null) {
                                    attachmentIdSet.add(orgComponent.Attachment_ID__c);
                                }
                            }

                            List<Attachment> attachmentList = new List<Attachment>();

                            Map<Id, Attachment> attachmentMap = new Map<Id, Attachment>([
                                SELECT Id
                                FROM Attachment
                                WHERE Id IN : attachmentIdSet
                            ]);

                            Map<String, Attachment> keyAttachmentMap = new Map<String, Attachment>();

                            for (Org_Component__c orgComponent : orgComponentList) {
                                if (orgComponent.Label__c == null) {
                                    logger.addLine(orgComponent.Component_Type__c + ' ' + orgComponent.Name__c + ' cannot be processed, not required metadata info.');
                                    continue;
                                }

                                Zippex zip = new Zippex();

                                if (orgComponent.Component_Type__c == 'AuraDefinitionBundle' || orgComponent.Component_Type__c == 'LightningComponentBundle' || orgComponent.Component_Type__c == 'ExperienceBundle' || orgComponent.Component_Type__c == 'WaveTemplateBundle') {
                                    for (String name : zippex.zipFileMap.keySet()) {
                                        if (name.startsWith(orgComponent.Label__c)) {
                                            zip.zipFileMap.put(name, zippex.getFileObjectZip(name));
                                        }
                                    }
                                } else {
                                    if (zippex.containsFile(orgComponent.Label__c)) {
                                        zip.zipFileMap.put(orgComponent.Label__c, zippex.getFileObjectZip(orgComponent.Label__c));
                                    }

                                    if (zippex.containsFile(orgComponent.Label__c + '-meta.xml')) {
                                        zip.zipFileMap.put(orgComponent.Label__c + '-meta.xml', zippex.getFileObjectZip(orgComponent.Label__c + '-meta.xml'));
                                    }
                                }

                                Integer crcTemp = 0;
                                for (String cmp : zip.zipFileMap.keySet()) {
                                    crcTemp += HexUtil.hexToIntLE(zip.zipFileMap.get(cmp).crc32);
                                }

                                String crc = HexUtil.intToHexLE(crcTemp, 4);

                                Attachment attachment;
                                if (orgComponent.Attachment_ID__c != null && attachmentMap.containsKey(orgComponent.Attachment_ID__c)) {
                                    attachment = attachmentMap.get(orgComponent.Attachment_ID__c);
                                } else {
                                    attachment = new Attachment();
                                    attachment.ParentId = organizationId;
                                }

                                attachment.Name = orgComponent.Name__c;
                                attachment.ContentType = 'application/zip';
                                attachment.Body = EncodingUtil.base64Decode(EncodingUtil.base64Encode(zip.getZipArchive()));
                                attachment.Description = orgComponent.Component_Type__c;
                                attachmentList.add(attachment);
                                keyAttachmentMap.put(orgComponent.Component_Type__c + '#' + orgComponent.Name__c, attachment);

                                orgComponent.CRC32__c = crc;
                                orgComponent.Is_Processed__c = false;
                                orgComponent.Is_Retrieved__c = true;
                                orgComponent.Status__c = 'Retrieved';
                                orgComponent.Async_Job_Id__c = null;
                                if (orgComponent.New_Revision_Counter__c != null) {
                                    orgComponent.Revision_Counter__c = orgComponent.New_Revision_Counter__c;
                                    orgComponent.New_Revision_Counter__c = null;
                                }
                            }

                            DatabaseUtils.upsertRecords(
                                attachmentList,
                                new List<Schema.DescribeFieldResult>{
                                    Attachment.Body.getDescribe(),
                                    Attachment.Name.getDescribe(),
                                    Attachment.Description.getDescribe(),
                                    Attachment.ContentType.getDescribe()
                                }
                            );

                            for (Org_Component__c orgComponent : orgComponentList) {
                                String key = orgComponent.Component_Type__c + '#' + orgComponent.Name__c;
                                if (keyAttachmentMap.containsKey(key)) {
                                    orgComponent.Attachment_ID__c = keyAttachmentMap.get(key).Id;
                                }
                            }

                            DatabaseUtils.upsertRecords(
                                orgComponentList,
                                new List<Schema.DescribeFieldResult>{
                                    Org_Component__c.Name__c.getDescribe(),
                                    Org_Component__c.API_Name__c.getDescribe(),
                                    Org_Component__c.Label__c.getDescribe(),
                                    Org_Component__c.Component_Type__c.getDescribe(),
                                    Org_Component__c.Last_Updated_By__c.getDescribe(),
                                    Org_Component__c.Last_Modified_Date__c.getDescribe(),
                                    Org_Component__c.Attachment_ID__c.getDescribe()
                                }
                            );

                            if (!keyAttachmentMap.isEmpty()) {
                                logger.addLine('Process complete for: ');
                                for (String key : keyAttachmentMap.keySet()) {
                                    logger.addLine(key);
                                }
                                logger.updateLog();
                            }
                        }
                    } else {
                        TrackingService.handleErrorSourceMembers(trackingSettingId, type);
                        logger.addLine('Error process components for \'' + type + '\': ' + retrieveResult.errorMessage);
                        logger.updateLog();
                    }
                }
            }

        } catch (Exception e) {
            logger.updateLog();
            for (String message : sizeErrorMessageLimit) {
                if (e.getMessage().contains(message)) {
                    isLimitError = true;
                }
            }
            if (!isLimitError) {
                TrackingService.handleErrorSourceMembers(trackingSettingId, type);
                logger.addLine('Error process metadata for \'' + type + '\': ' + e.getMessage());
                logger.updateLog();
            }
        }
    }

    public void finish(Database.BatchableContext param1) {
        try {
            TrackingService.checkIsTrackingIsProcess(trackingSettingId, param1);
            if (isLimitError && !limitAsyncJobComponentIdMap.isEmpty()) {
                System.enqueueJob(new TrackingLimitRequestQueueable(limitAsyncJobComponentIdMap, trackingSettingId, logger));
            } else {
                TrackingService.handleSuccessSettings(trackingSettingId);
                TrackingService.handleNotProcessedOrgComponents(trackingSettingId, logger);
                TrackingService.handleSuccessLog(logger, 'Retrieve completed.');
                if (!Test.isRunningTest()) TrackingService.nextRetrieve(trackingSettingId, false);
            }
        } catch (Exception e) {
            TrackingService.handleSuccessSettings(trackingSettingId);
            TrackingService.handleNotProcessedOrgComponents(trackingSettingId, logger);
            TrackingService.handleError(trackingSettingId, logger, 'Error while finish process');
            if (!Test.isRunningTest()) TrackingService.nextRetrieve(trackingSettingId, false);
        }
    }


    private Boolean getIsLimitTooLarge(MetadataService.RetrieveResult retrieveResult) {
        if (retrieveResult == null
                || retrieveResult.errorMessage == null) return false;
        for (String message : tooLargeLimitList) {
            if (retrieveResult.errorMessage.contains(message)) return true;
        }
        return false;
    }

    private void createTooLargeComponent(Org_Component__c orgComponent) {
        orgComponent.Is_Processed__c = false;
        orgComponent.Status__c = 'Too Large';
        orgComponent.Async_Job_Id__c = null;
        orgComponent.Attachment_ID__c = null;

        if (orgComponent.New_Revision_Counter__c != null) {
            orgComponent.Revision_Counter__c = orgComponent.New_Revision_Counter__c;
            orgComponent.New_Revision_Counter__c = null;
        }

        DatabaseUtils.updateRecord(
                orgComponent,
                new List<Schema.DescribeFieldResult>{
                        Org_Component__c.Is_Processed__c.getDescribe(),
                        Org_Component__c.Status__c.getDescribe(),
                        Org_Component__c.Revision_Counter__c.getDescribe(),
                        Org_Component__c.New_Revision_Counter__c.getDescribe()
                }
        );
    }

}