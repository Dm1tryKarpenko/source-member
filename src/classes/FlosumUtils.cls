global with sharing class FlosumUtils
{
    private Credentials__c credentials = Credentials__c.getOrgDefaults();
    
    global AuthenticateDetails authenticateAnOrg(String organisationId)
    {
        AuthenticateDetails authResult = new AuthenticateDetails();
        authResult.isSuccess = false;
        authResult.needToUpdateDetails = false;
        DatabaseUtils.checkObjectsAndFieldsAccessibility(new Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>>{
                Organisation__c.getSObjectType().getDescribe() => new List<Schema.DescribeFieldResult>{
                        Organisation__c.Username__c.getDescribe()
                }
        });
        List<Organisation__c> orgLi = [SELECT Id,Username__c,Name from Organisation__c Where Id=:organisationId LIMIT 1];
        if(orgLi != null && orgLi.size() > 0)
        {
            Boolean hasRetreivePermission = false;
            hasRetreivePermission = FlosumSecurity.checkRetrievePermission(orgLi[0].id);
            if(!hasRetreivePermission)
            {
                authResult.errorMessage = 'You do not have org permission.';
                return authResult;
            }
            if(hasRetreivePermission)
            {
                DatabaseUtils.checkObjectsAndFieldsAccessibility(new Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>>{
                        AuthorizationDetails__c.getSObjectType().getDescribe() => new List<Schema.DescribeFieldResult>{
                                AuthorizationDetails__c.AuthorisedUser__c.getDescribe(),
                                AuthorizationDetails__c.Name.getDescribe(),
                                AuthorizationDetails__c.AccessToken__c.getDescribe(),
                                AuthorizationDetails__c.RefreshToken__c.getDescribe(),
                                AuthorizationDetails__c.InstanceURL__c.getDescribe(),
                                AuthorizationDetails__c.Connected_App_Type__c.getDescribe()
                        }
                });
                List<AuthorizationDetails__c> detailLi = [SELECT AuthorisedUser__c, Name, AccessToken__c, RefreshToken__c, InstanceURL__c, Connected_App_Type__c
                                                            FROM AuthorizationDetails__c WHERE AuthorisedUser__c=:orgLi[0].Username__c LIMIT 1];
                if(detailLi != null && detailLi.size() > 0)
                {
                    try
                    {
                        //Authentication with access token.
                        String endpoint = detailLi[0].InstanceURL__c+'/services/data/v51.0/';
                        HttpRequest httpReq = new HttpRequest();
                        httpReq.setEndpoint(endpoint);
                        httpReq.setHeader('Authorization', 'Bearer '+detailLi[0].AccessToken__c);
                        httpReq.setMethod('GET');
                        httpReq.setTimeout(120000);
                        Http pipe = new Http();
                        HttpResponse res = pipe.send(httpReq);
                        System.debug(res.getStatusCode());
                        if(res.getStatusCode() == 200) 
                        {
                            String response = res.getBody();
                            Map<String,Object> params =(Map<String,Object>)JSON.deserializeUntyped(response);
                            if(params.containsKey('Error'))
                            {
                                //Authentication Failed.
                                authResult.isSuccess = false;
                            }
                            else
                            {
                                //Return in case of success.
                                authResult.isSuccess = true;
                                AuthorizationDetails__c det = new AuthorizationDetails__c();
                                det.AuthorisedUser__c = detailLi[0].AuthorisedUser__c;
                                det.Name = detailLi[0].Name;
                                det.AccessToken__c = detailLi[0].AccessToken__c;
                                det.RefreshToken__c = detailLi[0].RefreshToken__c;
                                det.InstanceURL__c = detailLi[0].InstanceURL__c;
                                det.Connected_App_Type__c = detailLi[0].Connected_App_Type__c;
                                authResult.details = det;
                            }
                        }
                        if(!authResult.isSuccess)
                        {
                            //Authentication with refresh token.
                            endpoint = detailLi[0].InstanceURL__c+'/services/oauth2/token?grant_type=refresh_token'+
                                        '&refresh_token='+detailLi[0].RefreshToken__c;
                            if(detailLi[0].Connected_App_Type__c == FlosumConstants.AUTO_AUTH)
                            {
                                endpoint += '&client_id='+credentials.Consumer_Key_Auto__c+
                                            '&client_secret='+credentials.Consumer_Secret_Auto__c;
                            }
                            else
                            {
                                endpoint += '&client_id='+credentials.ConsumerKey__c+
                                            '&client_secret='+credentials.ConsumerSecret__c;
                            }
                            httpReq = new HttpRequest();
                            httpReq.setEndpoint(endpoint);
                            httpReq.setMethod('POST');
                            httpReq.setTimeout(120000);
                            pipe = new Http();
                            res = new HttpResponse();
                            res = pipe.send(httpReq);
                            if(res.getStatusCode() == 200)
                            {
                                String response = res.getBody();
                                if(response != null && response != '')
                                {
                                    Map<String,Object> params = (Map<String,Object>)JSON.deserializeUntyped(response);
                                    if(params.containsKey('access_token') && params.get('access_token') != null)
                                    {
                                        detailLi[0].AccessToken__c = (String) params.get('access_token');
                                        if(params.containsKey('refresh_token') && params.get('refresh_token') != null)
                                            detailLi[0].RefreshToken__c = (String) params.get('refresh_token');

                                        AuthorizationDetails__c det = new AuthorizationDetails__c();
                                        det.AuthorisedUser__c = detailLi[0].AuthorisedUser__c;
                                        det.Name = detailLi[0].Name;
                                        det.AccessToken__c = detailLi[0].AccessToken__c;
                                        det.RefreshToken__c = detailLi[0].RefreshToken__c;
                                        det.InstanceURL__c = detailLi[0].InstanceURL__c;
                                        det.Connected_App_Type__c = detailLi[0].Connected_App_Type__c;
                                        authResult.details = det;
                                        authResult.isSuccess = true;
                                        authResult.needToUpdateDetails = true;
                                    }
                                }
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        authResult.errorMessage = ex.getMessage();
                    }
                }
            }
        }
        if(authResult.isSuccess)
            authResult.infoMessage = 'Authentication Successful.';
        else if(authResult.errorMessage == null)
            authResult.errorMessage = FlosumConstants.SESSION_EXPIRE;
        return authResult;
    }
    
    global class AuthenticateDetails
    {
        global boolean needToUpdateDetails;
        global boolean isSuccess;
        global String errorMessage;
        global String infoMessage;
        global AuthorizationDetails__c details;
    }
    
    /**
     * @description                             method for creating Process_Settings__c (custom settings)
     * @param Map<String, String> newSettings   custom setting name TO value
     */
    public static void setProcessSettings(Map<String, String> newSettings) {
        try {
            Map<String, Process_Settings__c> result = new Map<String, Process_Settings__c>();
            Map<String, Process_Settings__c> allSettings = Process_Settings__c.getAll();

            for (String settingKey : newSettings.keySet()) {
                if (settingKey != '' && settingKey != 'null') {
                    if (allSettings.containsKey(settingKey)) {
                        Process_Settings__c itemTS = allSettings.get(settingKey);
                        itemTS.Value__c = newSettings.get(settingKey);
                        result.put(settingKey, itemTS);
                    }
                    else {
                        result.put(settingKey, new Process_Settings__c(
                                Name = settingKey,
                                Value__c = newSettings.get(settingKey)
                        ));
                    }
                }
            }
            if (!result.isEmpty()){
                DatabaseUtils.upsertCustomSettingRecords(
                        result.values(),
                        new List<Schema.DescribeFieldResult>{
                                Process_Settings__c.Name.getDescribe(),
                                Process_Settings__c.Value__c.getDescribe()
                        }
                );
            }
        } catch (Exception e) { System.debug(e.getMessage()); }
    }

    public static String runMaintenanceScheduler() {
        try {
            List <String> state = new List<String>{
                    'WAITING', 'ACQUIRED', 'EXECUTING', 'PAUSED'
            };

            DatabaseUtils.checkObjectsAndFieldsAccessibility(new Map<Schema.DescribeSObjectResult, List<Schema.DescribeFieldResult>>{
                    CronTrigger.getSObjectType().getDescribe() => new List<Schema.DescribeFieldResult>{
                            CronTrigger.CronJobDetailId.getDescribe()
                    },
                    CronJobDetail.getSObjectType().getDescribe() => new List<Schema.DescribeFieldResult>{
                            CronJobDetail.Name.getDescribe()
                    }
            });
            List<CronTrigger> cronList = [
                    SELECT Id, CronJobDetailId, CronJobDetail.Name
                    FROM CronTrigger
                    WHERE CronJobDetail.Name = 'FlosumMaintenance' AND State IN :state
            ];
            if(!cronList.isEmpty()) return null;
            String chronExpression = '0 0 2 1/1 * ? *';
            String jobId = System.schedule('FlosumMaintenance', chronExpression, new FlosumMaintenanceScheduler());
            return jobId;
        } catch (Exception e) {
            return null;
        }
    }

    public class LWCLookupSearchResultWrapper implements Comparable {
        protected Id id;
        protected String sObjectType;
        protected String icon;
        protected String title;
        protected String subtitle;

        public LWCLookupSearchResultWrapper() {
        }

        public LWCLookupSearchResultWrapper(Id id, String sObjectType, String icon, String title, String subtitle) {
            this.id = id;
            this.sObjectType = sObjectType;
            this.icon = icon;
            this.title = title;
            this.subtitle = subtitle;
        }

        @AuraEnabled
        public Id getId() {
            return id;
        }

        @AuraEnabled
        public String getSObjectType() {
            return sObjectType;
        }

        @AuraEnabled
        public String getIcon() {
            return icon;
        }

        @AuraEnabled
        public String getTitle() {
            return title;
        }

        @AuraEnabled
        public String getSubtitle() {
            return subtitle;
        }

        /**
         * Allow to sort search results based on title
         */
        public Integer compareTo(Object compareTo) {
            LWCLookupSearchResultWrapper other = (LWCLookupSearchResultWrapper) compareTo;
            if (this.getTitle() == null) {
                return (other.getTitle() == null) ? 0 : 1;
            }
            if (other.getTitle() == null) {
                return -1;
            }
            return this.getTitle().compareTo(other.getTitle());
        }
    }

}