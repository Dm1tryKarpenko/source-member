/**
 * Created by vladislav on 15.09.21.
 */

@IsTest
public with sharing class TrackingServiceTest {

    @TestSetup
    public static void setup() {
        Organisation__c org = new Organisation__c(
                Username__c = 'test@test.com',
                User_Full_Name__c = 'test person',
                OwnerId = UserInfo.getUserId()
        );
        insert org;

        Tracking_Setting__c trackingOrganizationSetting = new Tracking_Setting__c();
        trackingOrganizationSetting.Organization__c = org.Id;
        trackingOrganizationSetting.Is_Processed__c = false;
        trackingOrganizationSetting.Is_Enabled__c = true;
        trackingOrganizationSetting.Frequency__c = 1;
        trackingOrganizationSetting.Last_Start_Retrieval__c = Datetime.now().addDays(-1);
        trackingOrganizationSetting.Metadata_Types__c = TrackingRequestRetrieveBatchTest.ALL_METATYPES;
        insert trackingOrganizationSetting;
    }

    @IsTest
    public static void createLogTest() {
        Organisation__c organisation = [SELECT Id FROM Organisation__c WHERE Username__c = : 'test@test.com'];

        List<Tracking_Setting__c> trackingOrganizationSettingList = [
                SELECT Id, Name, Is_Processed__c, Organization__c, Organization__r.Name, Current_Jobs__c, Metadata_Types__c
                FROM Tracking_Setting__c
                WHERE Organization__c =: organisation.Id
        ];

        if (!trackingOrganizationSettingList.isEmpty()) {
            Metadata_Log__c metadataLog = TrackingService.createLog(
                    organisation.Id,
                    trackingOrganizationSettingList.get(0).Id,
                    TrackingService.PROCESS_TYPE_RETRIEVE_ORGANIZATION, TrackingService.TRACKING_ORGANIZATION_LOG_NAME
            );
            System.assert(String.isNotBlank(metadataLog.Id));
        } else {
            throw new FlosumException('Tracking setting not found!');
        }
    }

    @IsTest
    public static void makeRequestRetrieveAndCheckDependencyTest() {
        Test.setMock(WebServiceMock.class, new TrackingOrganizationControllerTest.WebServiceMockImpl(new Map<String, Object> {
                'isDependency' => true
        }));
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();

        TrackingService.RetrieveComponentWrapper retrieveComponentWrapperProfile = new TrackingService.RetrieveComponentWrapper(
                'Profile',
                new List<String> { 'TestComponent0', 'TestComponent1' }
        );
        TrackingService.RetrieveComponentWrapper retrieveComponentWrapperCustomObjectTranslation = new TrackingService.RetrieveComponentWrapper(
                'CustomObjectTranslation',
                new List<String> { 'TestComponent-0', 'TestComponent-1' }
        );
        TrackingService.RetrieveComponentWrapper retrieveComponentWrapperTranslations = new TrackingService.RetrieveComponentWrapper(
                'Translations',
                new List<String> { 'TestComponent0', 'TestComponent1' }
        );

        Test.startTest();
        List<String> asyncJobListProfile = TrackingService.makeRequestRetrieve(
                testDataContainer.accessToken,
                testDataContainer.instanceUrl,
                new List<TrackingService.RetrieveComponentWrapper> { retrieveComponentWrapperProfile }
        );
        List<String> asyncJobListCustomObjectTranslation = TrackingService.makeRequestRetrieve(
                testDataContainer.accessToken,
                testDataContainer.instanceUrl,
                new List<TrackingService.RetrieveComponentWrapper> { retrieveComponentWrapperCustomObjectTranslation }
        );
        List<String> asyncJobListTranslations = TrackingService.makeRequestRetrieve(
                testDataContainer.accessToken,
                testDataContainer.instanceUrl,
                new List<TrackingService.RetrieveComponentWrapper> { retrieveComponentWrapperTranslations }
        );
        Test.stopTest();

        System.assert(!asyncJobListProfile.isEmpty());
        System.assert(!asyncJobListCustomObjectTranslation.isEmpty());
        System.assert(!asyncJobListTranslations.isEmpty());
    }

    @IsTest
    public static void makeLimitMetadataRequest() {
        Test.setMock(WebServiceMock.class, new TrackingOrganizationControllerTest.WebServiceMockImpl());
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();

        TrackingOrganizationControllerTest.createTestOrgComponents(testDataContainer.organisationId, testDataContainer.trackingSettingId, 50);

        List<Org_Component__c> componentList = [SELECT Id FROM Org_Component__c];

        List<String> componentIdList = new List<String>();
        for(Org_Component__c component: componentList) {
            componentIdList.add(component.Id);
        }

        Test.startTest();
        List<String> resultList = TrackingService.makeLimitMetadataRequest(
                testDataContainer.accessToken,
                testDataContainer.instanceUrl,
                componentIdList,
                testDataContainer.logger);
        Test.stopTest();

        System.assertEquals(2, resultList.size());
    }

    @IsTest
    public static void callSourceMemberResponseTest() {
        Test.setMock(HttpCalloutMock.class, new TrackingOrganizationControllerTest.TestMock(TrackingRequestRetrieveBatchTest.ENDPOINT_RESPONSE_MAP));
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();

        Test.startTest();
        TrackingService.ApexRestResponseWrapper responseWrapper = TrackingService.callSourceMemberResponse(
                testDataContainer.accessToken,
                TrackingRequestRetrieveBatchTest.HTTP_SOURCE_MEMBER_SIZE_ENDPOINT
        );
        Test.stopTest();

        System.assert(!responseWrapper.records.isEmpty());
        System.assert(responseWrapper.records.get(0).SourceMemberSize > 0);
    }

    @IsTest
    public static void callSourceMemberRecordsTest() {
        Test.setMock(HttpCalloutMock.class, new TrackingOrganizationControllerTest.TestMock(TrackingRequestRetrieveBatchTest.ENDPOINT_RESPONSE_MAP));
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();

        TrackingOrganizationControllerTest.createTestOrgComponents(testDataContainer.organisationId, testDataContainer.trackingSettingId, 50);

        Test.startTest();
        TrackingService.SourceMemberPacksWrapper packsWrapper = TrackingService.callSourceMemberRecords(
                testDataContainer.trackingSettingId,
                testDataContainer.accessToken,
                TrackingRequestRetrieveBatchTest.HTTP_SOURCE_MEMBER_ENDPOINT
        );
        Test.stopTest();

        System.assertNotEquals(null, packsWrapper);
        System.assert(!packsWrapper.changedList.isEmpty());
    }

    @IsTest
    public static void handleSuccessSettingsTest() {
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();
        enableProcessTrackingSetting(testDataContainer.trackingSettingId);

        Test.startTest();
        TrackingService.handleSuccessSettings(testDataContainer.trackingSettingId);
        Test.stopTest();

        TrackingOrganizationControllerTest.TestResultDataContainer resultDataContainer =
                new TrackingOrganizationControllerTest.TestResultDataContainer(testDataContainer.trackingSettingId);

        System.assertEquals(false, resultDataContainer.isProcessed);
        System.assert(String.isBlank(resultDataContainer.currentJobs));
    }

    @IsTest
    public static void handleNotProcessedOrgComponentsTest() {
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();
        enableProcessTrackingSetting(testDataContainer.trackingSettingId);
        TrackingOrganizationControllerTest.createTestOrgComponents(testDataContainer.organisationId, testDataContainer.trackingSettingId, 50);

        Test.startTest();
        TrackingService.handleNotProcessedOrgComponents(testDataContainer.trackingSettingId, testDataContainer.logger);
        Test.stopTest();

        TrackingOrganizationControllerTest.TestResultDataContainer resultDataContainer =
                new TrackingOrganizationControllerTest.TestResultDataContainer(testDataContainer.trackingSettingId);

        System.assertEquals(50, resultDataContainer.sizeOrgComponentsError);
    }

    @IsTest
    public static void handleSuccessLogTest() {
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();
        enableProcessTrackingSetting(testDataContainer.trackingSettingId);
        TrackingOrganizationControllerTest.createTestOrgComponents(testDataContainer.organisationId, testDataContainer.trackingSettingId, 50);

        Test.startTest();
        TrackingService.handleSuccessLog(testDataContainer.logger, 'Test handleSuccessLog');
        Test.stopTest();

        List<Metadata_Log__c> metadataLogList = [SELECT Name, Status__c FROM Metadata_Log__c WHERE Id = : testDataContainer.logger.parentId];

        System.assertEquals('Completed', metadataLogList.get(0).Status__c);
    }

    @IsTest
    public static void handleErrorLogTest() {
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();
        enableProcessTrackingSetting(testDataContainer.trackingSettingId);

        Test.startTest();
        TrackingService.handleErrorLog(testDataContainer.logger, 'Test handleErrorLog');
        Test.stopTest();

        List<Metadata_Log__c> metadataLogList = [SELECT Name, Status__c FROM Metadata_Log__c WHERE Id = : testDataContainer.logger.parentId];

        System.assertEquals('Exception', metadataLogList.get(0).Status__c);
    }

    @IsTest
    public static void handleErrorTest() {
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();
        enableProcessTrackingSetting(testDataContainer.trackingSettingId);
        TrackingOrganizationControllerTest.createTestOrgComponents(testDataContainer.organisationId, testDataContainer.trackingSettingId, 50);

        Test.startTest();
        TrackingService.handleError(testDataContainer.trackingSettingId, 'ApexClass');
        Test.stopTest();

        TrackingOrganizationControllerTest.TestResultDataContainer resultDataContainer =
                new TrackingOrganizationControllerTest.TestResultDataContainer(testDataContainer.trackingSettingId);

        System.assertEquals(false, resultDataContainer.isProcessed);
        System.assertEquals(50, resultDataContainer.sizeOrgComponentsError);
    }

    @IsTest
    public static void getWhereClauseTest() {
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();
        List<Tracking_Setting__c> trackingSettingList = [SELECT Metadata_Types__c FROM Tracking_Setting__c WHERE Id =: testDataContainer.trackingSettingId];

        Test.startTest();
        String lastId = TrackingService.getWhereClause('LastIdTest', trackingSettingList.get(0));
        String lastIdBlank = TrackingService.getWhereClause('', trackingSettingList.get(0));
        String componentIdSourceMemberList = TrackingService.getWhereClause(new List<String> { 'TestId1' }, trackingSettingList.get(0));
        String componentIdSourceMemberListEmpty = TrackingService.getWhereClause(new List<String>(), trackingSettingList.get(0));
        Test.stopTest();

        System.assert(lastId.contains('LastIdTest'));
        System.assert(!lastIdBlank.contains('LastIdTest'));
        System.assert(componentIdSourceMemberList.contains('TestId1'));
        System.assert(!componentIdSourceMemberListEmpty.contains('TestId1'));
    }

    @IsTest
    public static void getAndCreateProcessedSourceMemberRecordsTest() {
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();
        enableProcessTrackingSetting(testDataContainer.trackingSettingId);
        TrackingOrganizationControllerTest.createTestOrgComponents(testDataContainer.organisationId, testDataContainer.trackingSettingId, 50);

        List<TrackingService.SourceMemberWrapper> sourceMemberWrapperList = new List<TrackingService.SourceMemberWrapper>();
        List<Org_Component__c> componentList = [
                SELECT Name, Name__c, Component_Type__c, Last_Updated_By__c, Revision_Counter__c, Is_Deleted__c, Last_Modified_Date__c, Status__c, Source_Member_Id__c
                FROM Org_Component__c
                WHERE Tracking_Setting__c =: testDataContainer.trackingSettingId
        ];

        for(Org_Component__c orgComponent : componentList) {
            sourceMemberWrapperList.add(new TrackingService.SourceMemberWrapper(orgComponent));
        }

        delete [SELECT Id FROM Org_Component__c WHERE Tracking_Setting__c =: testDataContainer.trackingSettingId LIMIT 20];

        Test.startTest();
        Map<String,List<Org_Component__c>> resultComponentMap = TrackingService.getAndCreateProcessedSourceMemberRecords(
                sourceMemberWrapperList,
                testDataContainer.trackingSettingId,
                testDataContainer.organisationId
        );
        Test.stopTest();

        System.assertEquals(20, resultComponentMap.get('insertOrgComponentList').size());
        System.assertEquals(30, resultComponentMap.get('updateOrgComponentList').size());
    }

    @IsTest
    public static void nextRetrieveTest() {
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();

        Test.startTest();
        String jobId = TrackingService.nextRetrieve(testDataContainer.trackingSettingId, false);
        String jobIdImmidiately = TrackingService.nextRetrieve(testDataContainer.trackingSettingId, true);
        Test.stopTest();

        System.assertEquals('TestJobId', jobId);
        System.assertEquals('TestJobId', jobIdImmidiately);
    }


    public static void enableProcessTrackingSetting(String trackingSettingId) {
        List<Tracking_Setting__c> trackingSettingList = [
                SELECT Is_Processed__c, Current_Jobs__c
                FROM Tracking_Setting__c
                WHERE Id =: trackingSettingId
        ];
        if (!trackingSettingList.isEmpty()) {
            trackingSettingList.get(0).Is_Processed__c = true;
            trackingSettingList.get(0).Current_Jobs__c = 'OneJobs,TwoJobs';
            update trackingSettingList;
        } else {
            throw new FlosumException('Tracking setting not found!');
        }
    }
}