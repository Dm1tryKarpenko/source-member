/**
 * Created by vladislav on 15.09.21.
 */
@IsTest
public with sharing class TrackingLimitRequestQueueableTest {

    @TestSetup
    public static void setup() {
        Organisation__c org = new Organisation__c(
                Username__c = 'test@test.com',
                User_Full_Name__c = 'test person',
                OwnerId = UserInfo.getUserId()
        );
        insert org;

        Tracking_Setting__c trackingOrganizationSetting = new Tracking_Setting__c();
        trackingOrganizationSetting.Organization__c = org.Id;
        trackingOrganizationSetting.Is_Processed__c = true;
        trackingOrganizationSetting.Is_Enabled__c = true;
        DatabaseUtils.insertRecord(
                trackingOrganizationSetting,
                new List<Schema.DescribeFieldResult>{
                        Tracking_Setting__c.Organization__c.getDescribe(),
                        Tracking_Setting__c.Is_Processed__c.getDescribe(),
                        Tracking_Setting__c.Is_Enabled__c.getDescribe()
                }
        );
    }

    @IsTest
    public static void limitTest() {
        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();
        TrackingOrganizationControllerTest.createTestOrgComponents(testDataContainer.organisationId,testDataContainer.trackingSettingId,100);

        System.assert(String.isBlank([SELECT Current_Jobs__c FROM Tracking_Setting__c WHERE Id =: testDataContainer.trackingSettingId].Current_Jobs__c));
        Test.startTest();
        System.enqueueJob(new TrackingLimitRequestQueueable(
                new Map<String, List<String>>{ 'JobId' => new List<String> { [SELECT Id From Org_Component__c LIMIT 1].Id } },
                testDataContainer.trackingSettingId,
                testDataContainer.logger
        ));
        Test.stopTest();
        System.assert(String.isNotBlank([SELECT Current_Jobs__c FROM Tracking_Setting__c WHERE Id =: testDataContainer.trackingSettingId].Current_Jobs__c));
    }
}