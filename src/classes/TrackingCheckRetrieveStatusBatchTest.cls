/**
 * Created by vladislav on 5.08.21.
 */

@IsTest
public class TrackingCheckRetrieveStatusBatchTest {
    public static final Integer SIZE_INITIAL_ORG_COMPONENTS = 10;
    @TestSetup
    public static void setup() {
        Organisation__c org = new Organisation__c(
                Username__c = 'test@test.com',
                Name = 'test',
                User_Full_Name__c = 'test person',
                OwnerId = UserInfo.getUserId()
        );
        insert org;

        Tracking_Setting__c trackingOrganizationSetting = new Tracking_Setting__c();
        trackingOrganizationSetting.Organization__c = org.Id;
        trackingOrganizationSetting.Is_Processed__c = true;
        trackingOrganizationSetting.Metadata_Types__c = TrackingRequestRetrieveBatchTest.ALL_METATYPES;
        trackingOrganizationSetting.Current_Jobs__c = '0MZ0t00000JHCAgGAP#ApexClass,0MZ0t00000JHCAiGAP#Profile';

        insert trackingOrganizationSetting;

        TrackingOrganizationControllerTest.createTestOrgComponents(org.id, trackingOrganizationSetting.id, SIZE_INITIAL_ORG_COMPONENTS);

    }

    @IsTest
    public static void standardLaunch() {
        Test.setMock(WebServiceMock.class, new TrackingOrganizationControllerTest.WebServiceMockImpl());

        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();

        Test.startTest();
        Database.executeBatch(new TrackingCheckRetrieveStatusBatch(
                testDataContainer.trackingSettingId,
                testDataContainer.logger
        ), 75);
        Test.stopTest();

        TrackingOrganizationControllerTest.TestResultDataContainer results =
                new TrackingOrganizationControllerTest.TestResultDataContainer(testDataContainer.trackingSettingId);

       System.assertEquals(true, results.isProcessed);
       System.assertEquals(SIZE_INITIAL_ORG_COMPONENTS, results.sizeOrgComponentsAll);
    }

    @IsTest
    public static void notCompleted() {
        Test.setMock(WebServiceMock.class, new TrackingOrganizationControllerTest.WebServiceMockImpl(new Map<String, Object> {
                'isLimitError' => false,
                'isDone' => false
        }));

        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();

        Test.startTest();
        Database.executeBatch(new TrackingCheckRetrieveStatusBatch(
                testDataContainer.trackingSettingId,
                testDataContainer.logger
        ), 75);
        Test.stopTest();

        TrackingOrganizationControllerTest.TestResultDataContainer results =
                new TrackingOrganizationControllerTest.TestResultDataContainer(testDataContainer.trackingSettingId);

        System.assertEquals(true, results.isProcessed);
        System.assertEquals(SIZE_INITIAL_ORG_COMPONENTS, results.sizeOrgComponentsAll);
    }

    @IsTest
    public static void trackingSettingEmpty() {
        Test.setMock(WebServiceMock.class, new TrackingOrganizationControllerTest.WebServiceMockImpl());

        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();

        String exceptionMessage;

        Test.startTest();
        Database.executeBatch(new TrackingCheckRetrieveStatusBatch(
                '',
                testDataContainer.logger
        ), 75);
        Test.stopTest();

        TrackingOrganizationControllerTest.TestResultDataContainer results =
                new TrackingOrganizationControllerTest.TestResultDataContainer(testDataContainer.trackingSettingId);

        System.assertEquals(true, results.isProcessed);
        System.assertEquals(SIZE_INITIAL_ORG_COMPONENTS, results.sizeOrgComponentsAll);
    }

    @IsTest
    public static void trackingSettingWrong() {
        Test.setMock(WebServiceMock.class, new TrackingOrganizationControllerTest.WebServiceMockImpl());

        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();

        Test.startTest();
        Database.executeBatch(new TrackingCheckRetrieveStatusBatch(
                '1234',
                testDataContainer.logger
        ), 75);
        Test.stopTest();

        TrackingOrganizationControllerTest.TestResultDataContainer results =
                new TrackingOrganizationControllerTest.TestResultDataContainer(testDataContainer.trackingSettingId);

        System.assertEquals(true, results.isProcessed);
        System.assertEquals(SIZE_INITIAL_ORG_COMPONENTS, results.sizeOrgComponentsAll);
    }

    @IsTest
    public static void loggerNull() {
        Test.setMock(WebServiceMock.class, new TrackingOrganizationControllerTest.WebServiceMockImpl());

        TrackingOrganizationControllerTest.TestDataContainer testDataContainer = new TrackingOrganizationControllerTest.TestDataContainer();

        String exceptionMessage;

        Test.startTest();
        try {
            TrackingCheckRetrieveStatusBatch checkRetrieveStatusBatch = new TrackingCheckRetrieveStatusBatch(
                    testDataContainer.trackingSettingId,
                    null
            );
        } catch (Exception e) {
            exceptionMessage = e.getMessage();
        }
        Test.stopTest();

        TrackingOrganizationControllerTest.TestResultDataContainer results =
                new TrackingOrganizationControllerTest.TestResultDataContainer(testDataContainer.trackingSettingId);

        System.assertEquals('logger couldn\'t be null', exceptionMessage);
        System.assertEquals(true, results.isProcessed);
        System.assertEquals(SIZE_INITIAL_ORG_COMPONENTS, results.sizeOrgComponentsAll);
    }
}