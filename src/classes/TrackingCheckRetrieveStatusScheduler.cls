public with sharing class TrackingCheckRetrieveStatusScheduler implements Schedulable {

    private static final String SCHED_EXPRESSION = '{0} {1} {2} {3} {4} ? {5}';

    private String trackingSettingId;
    private TrackingService.Logger logger;

    public TrackingCheckRetrieveStatusScheduler(String trackingSettingId, TrackingService.Logger logger) {
        this.trackingSettingId = trackingSettingId;
        this.logger = logger;
    }

    public void execute(SchedulableContext context) {
        if (!Test.isRunningTest()) {
            Database.executeBatch(new TrackingCheckRetrieveStatusBatch(trackingSettingId, logger), 50);
        }
        System.abortJob(context.getTriggerId());
    }

    public static List<String> initialize(String trackingSettingId, String organizationId, TrackingService.Logger logger) {
        return new TrackingCheckRetrieveStatusScheduler(trackingSettingId, logger).schedule(10, organizationId);
    }

    private List<String> schedule(Integer secondsFromNow, String organizationId) {
        List<String> ids = new List<String>();
        Datetime nextRun = Datetime.now().addSeconds(secondsFromNow);
        String scheduleExpression = String.format(
            SCHED_EXPRESSION,
            new List<Integer>{
                nextRun.second(),
                nextRun.minute(),
                nextRun.hour(),
                nextRun.day(),
                nextRun.month(),
                nextRun.year()
            }
        );
        try {
            String jobName = 'Tracking Check Retrieve ' + organizationId;
            ids.add(System.schedule(Test.isRunningTest() ? jobName + Datetime.now().getTime() : jobName, scheduleExpression, this));
        } catch (Exception e) {
            System.debug(LoggingLevel.ERROR, e.getMessage());
        }
        return ids;
    }

}